import { NavigationContainer } from '@react-navigation/native'
import Amplify from 'aws-amplify'
// @ts-ignore
import { withAuthenticator } from 'aws-amplify-react-native'
import React, { FC } from 'react'
import AWSConfig from './src/aws-exports'
import Navigation from './src/navigation/Navigation'

Amplify.configure({
  ...AWSConfig,
  Analytics: {
    disabled: true,
  },
})

const App: FC = () => {
  return (
    <NavigationContainer>
      <Navigation />
    </NavigationContainer>
  )
}

export default withAuthenticator(App)
