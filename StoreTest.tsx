import * as React from 'react'
import { StyleSheet, Text, TextStyle, ViewStyle } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { usePostsActions, usePostsState } from './src/store/posts.slice'

const StoreTest: React.FC = () => {
  const { data, isLoading, hasError, error } = usePostsState()
  const { loadPostsAsync } = usePostsActions()

  React.useEffect(() => {
    loadPostsAsync()
  }, [loadPostsAsync])

  if (isLoading) return <Text>Loading...</Text>
  if (hasError) return <Text>{error}</Text>

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.display}>{JSON.stringify(data, null, 2)}</Text>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 16,
  } as ViewStyle,
  display: {
    backgroundColor: '#2d3436',
    color: '#dfe6e9',
    padding: 16,
  } as TextStyle,
})

export default StoreTest
