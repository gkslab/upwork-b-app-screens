import { useIsFocused } from '@react-navigation/native'
import { API, graphqlOperation } from 'aws-amplify'
import React, { FC, useState } from 'react'
import { FlatList, Image, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import Post from '../components/Post'
import PostRow from '../components/PostRow'
import { PUBLIC_TOPICS } from '../graphql/customQueries'

const PostList: React.FC<{
  posts: any[]
  cols: number
  children: (item: any) => JSX.Element
}> = React.memo(({ posts, children, cols }) => {
  return (
    <FlatList
      key={cols === 1 ? '_' : '#'}
      data={posts}
      keyExtractor={({ id }) => '_' + id.toString()}
      renderItem={({ item }) => children(item)}
      numColumns={cols}
    />
  )
})
PostList.displayName = 'PostList'

const SearchPosts: FC = () => {
  const [selectedMode, setSelectedMode] = useState<string>('public')
  const isFocused = useIsFocused()
  const [publicTopics, setPublicTopics] = React.useState([])
  const [followingPosts, setFollowingPosts] = React.useState([])

  const fetchTopics = async () => {
    // @ts-ignore
    const { data } = await API.graphql(graphqlOperation(PUBLIC_TOPICS))
    setPublicTopics(data?.listTopics?.items)
    setFollowingPosts(data?.listTopics?.items)
  }

  React.useEffect(() => {
    if (isFocused) {
      fetchTopics()
    }
  }, [isFocused])

  return (
    <View style={styles.container}>
      <View style={styles.btnsContainer}>
        <TouchableWithoutFeedback onPress={() => setSelectedMode('following')}>
          <View
            style={[
              styles.tabItem,
              {
                backgroundColor: selectedMode === 'following' ? '#FFE1A8' : 'transparent',
              },
            ]}
          >
            <Image source={require('../assets/users.png')} style={{ width: 30, height: 30 }} />
            <Text style={styles.tabText}>Following</Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback onPress={() => setSelectedMode('public')}>
          <View
            style={[
              styles.tabItem,
              {
                backgroundColor: selectedMode === 'public' ? '#FFE1A8' : 'transparent',
              },
            ]}
          >
            <Image source={require('../assets/world.png')} style={{ width: 22, height: 22 }} />
            <Text style={styles.tabText}>Public</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
      <View style={styles.line} />
      {selectedMode === 'public' ? (
        <PostList cols={2} posts={publicTopics}>
          {(item) => <Post item={item} />}
        </PostList>
      ) : (
        <PostList cols={1} posts={followingPosts}>
          {(item) => <PostRow item={item} />}
        </PostList>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  btnsContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    borderColor: '#C4C4C4',
    borderRadius: 10,
    borderWidth: 1,
    marginVertical: 10,
  },
  tabItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderRadius: 10,
  },
  tabText: {
    paddingHorizontal: 7,
  },
  line: {
    borderBottomColor: '#C4C4C488',
    borderBottomWidth: 1,
    marginHorizontal: -10,
    marginBottom: 20,
    marginTop: 5,
  },
})

export default SearchPosts
