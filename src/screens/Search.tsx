import React, { FC, useState } from 'react'
import { SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import SearchInput from '../components/SearchInput'
import SearchPosts from './SearchPosts'
import SearchProfile from './SearchProfile'

const Search: FC = () => {
  const [selected, setSelected] = useState<string>('profile')
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <SearchInput />
        <View style={styles.row}>
          <TouchableWithoutFeedback onPress={() => setSelected('profile')}>
            <Text
              style={[
                styles.txtButton,
                {
                  backgroundColor: selected === 'profile' ? '#C4C4C4' : 'transparent',
                },
              ]}
            >
              Profile
            </Text>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => setSelected('posts')}>
            <Text
              style={[
                styles.txtButton,
                {
                  backgroundColor: selected === 'posts' ? '#C4C4C4' : 'transparent',
                },
              ]}
            >
              Posts
            </Text>
          </TouchableWithoutFeedback>
        </View>
        {selected === 'profile' ? <SearchProfile /> : <SearchPosts />}
      </View>
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    backgroundColor: 'white',
  },
  row: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  txtButton: {
    marginHorizontal: 5,
    paddingHorizontal: 40,
    paddingVertical: 5,
    borderRadius: 10,
    borderColor: '#C4C4C4',
    borderWidth: 0.5,
    overflow: 'hidden',
  },
})

export default Search
