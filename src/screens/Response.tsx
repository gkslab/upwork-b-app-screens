import { useNavigation, useRoute } from '@react-navigation/native'
import { API, Auth, graphqlOperation, Storage } from 'aws-amplify'
import { Video } from 'expo-av'
import { Camera } from 'expo-camera'
import * as Permissions from 'expo-permissions'
import * as VideoThumbnails from 'expo-video-thumbnails'
import React, { FC, useEffect, useRef, useState } from 'react'
import {
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { CreatePostMutationVariables } from '../API'
import { createPost } from '../graphql/mutations'

const { height, width } = Dimensions.get('window')
const Response: FC = () => {
  const [videoPath, setVideoPath] = useState<string>('')
  const [selected, setSelected] = useState<number>()

  const [hasPermission, setHasPermission] = useState<any>(null)
  const [camera, setCamera] = useState<any>(null)
  const videoRef = useRef<any>()

  const [finished, setFinished] = useState<boolean>(false)
  const [isRecording, setIsRecording] = useState<boolean>(false)
  const [type, setType] = useState<any>(Camera.Constants.Type.front)

  const [imagePadding, setImagePadding] = useState<number>(0)
  const [ratio, setRatio] = useState<string>('4:3')
  const screenRatio = height / width
  const [isRatioSet, setIsRatioSet] = useState<boolean>(false)
  const timerRef = useRef<any>()
  const [progress, setProgress] = React.useState('')
  const [isUploading, setUploading] = React.useState(false)

  const navigation = useNavigation()
  const route = useRoute()
  const { sides } = route.params

  useEffect(() => {
    async function getCameraStatus() {
      const statusCamera = await Permissions.askAsync(Permissions.CAMERA)
      const statusAudio = await Permissions.askAsync(Permissions.AUDIO_RECORDING)
      setHasPermission(statusCamera.status === 'granted' && statusAudio.status === 'granted')
    }
    getCameraStatus()
    return () => clearTimeout(timerRef.current)
  }, [])

  const prepareRatio = async () => {
    let desiredRatio = '4:3'
    if (Platform.OS === 'android') {
      const ratios = await camera.getSupportedRatiosAsync()
      const distances: any = {}
      const realRatios: any = {}
      let minDistance = null
      for (const ratio of ratios) {
        const parts = ratio.split(':')
        const realRatio = parseInt(parts[0]) / parseInt(parts[1])
        realRatios[ratio] = realRatio
        const distance = screenRatio - realRatio
        distances[ratio] = realRatio
        if (minDistance == null) {
          minDistance = ratio
        } else {
          if (distance >= 0 && distance < distances[minDistance]) {
            minDistance = ratio
          }
        }
      }
      desiredRatio = minDistance
      const remainder = Math.floor((height - realRatios[desiredRatio] * width) / 2)
      setImagePadding(remainder / 2)
      setRatio(desiredRatio)
      setIsRatioSet(true)
    }
  }

  const setCameraReady = async () => {
    if (!isRatioSet) {
      await prepareRatio()
    }
  }

  const recordVideo = async () => {
    setFinished(false)
    setIsRecording(true)
    const timer = setTimeout(function () {
      if (!finished) stopRecording()
    }, 30000)
    timerRef.current = timer
    const data = await camera.recordAsync()
    if (data) {
      await videoRef.current.loadAsync({ uri: data.uri })
      await videoRef.current.playAsync()
      await videoRef.current.setIsLoopingAsync(true)
      setVideoPath(data.uri)
    }
    setIsRecording(false)
  }
  const stopRecording = async () => {
    if (!finished) {
      await camera.stopRecording()
      setFinished(true)
    }
  }

  const handleSubmit = async () => {
    try {
      setProgress('')
      setUploading(true)
      const videoResponse = await fetch(videoPath)
      const video = await videoResponse.blob()
      const videoExt = videoPath.split('.').pop()
      // @ts-ignore
      const { key: videoKey } = await Storage.put(`${makeId()}.${videoExt}`, video, {
        progressCallback(progress: any) {
          setProgress(`${Math.round((progress.loaded / progress.total) * 100)}`)
        },
      })

      const { uri: thumbnailPath } = await VideoThumbnails.getThumbnailAsync(videoPath, {
        time: 15,
      })
      const thumbnailResponse = await fetch(thumbnailPath)
      const thumbnail = await thumbnailResponse.blob()
      const thumbnailExt = thumbnailPath.split('.').pop()
      // @ts-ignore
      const { key: thumbnailKey } = await Storage.put(`${makeId()}.${thumbnailExt}`, thumbnail)
      // const thumbnailUrl = await Storage.get(thumbnailKey)

      const { id: userID } = await Auth.currentUserInfo()
      // const videoUrl = await Storage.get(videoKey)
      // @ts-ignore
      const { topicID } = route.params
      const side = sides[selected]

      const input = {
        userID,
        videoUrl: videoKey,
        topicID,
        side,
        thumbnail: thumbnailKey,
      }

      await API.graphql(
        graphqlOperation(createPost, {
          input,
        } as CreatePostMutationVariables),
      )

      setUploading(false)
      navigation.goBack()
    } catch (error) {
      alert('Failed to upload video')
      console.log('Failed to upload video', error)
      setUploading(false)
    }
  }

  if (hasPermission === null) {
    return (
      <View style={styles.information}>
        <Text>Waiting for camera permissions</Text>
      </View>
    )
  } else if (hasPermission === false) {
    return (
      <View style={styles.information}>
        <Text>No access to camera</Text>
      </View>
    )
  } else {
    return (
      <View style={styles.container}>
        {finished ? (
          <Video
            rate={1.0}
            volume={1.0}
            isMuted={false}
            resizeMode="cover"
            shouldPlay
            isLooping={true}
            style={[
              styles.cameraPreview,
              {
                marginTop: imagePadding,
                marginBottom: imagePadding,
              },
            ]}
            ref={videoRef}
          />
        ) : (
          <Camera
            type={type}
            style={[styles.cameraPreview, { marginTop: imagePadding, marginBottom: imagePadding }]}
            onCameraReady={setCameraReady}
            ratio={ratio}
            ref={(ref) => {
              setCamera(ref)
            }}
          />
        )}
        <View style={styles.btnsContainer}>
          {finished ? null : (
            <View style={styles.recordingContainer}>
              <View style={styles.switchCamContainer}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    setType(
                      type === Camera.Constants.Type.front
                        ? Camera.Constants.Type.back
                        : Camera.Constants.Type.front,
                    )
                  }
                >
                  <Image
                    source={require('../assets/cameraSwitch.png')}
                    style={{ width: 47, height: 38 }}
                  />
                </TouchableWithoutFeedback>
              </View>
              <View style={styles.recordingBtnContainer}>
                <TouchableWithoutFeedback
                  onLongPress={() => recordVideo()}
                  onPressOut={() => stopRecording()}
                >
                  <View
                    style={[
                      styles.circleStyle,
                      {
                        backgroundColor: isRecording ? '#FF5555' : '#FFFFFF44',
                      },
                    ]}
                  >
                    <View style={styles.innerFakeShadow}></View>
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={{ flex: 1 }} />
            </View>
          )}
          <View style={{ flexDirection: 'row', marginVertical: 5 }}>
            {(sides as any[]).map((side, index) => (
              <TouchableWithoutFeedback key={index} onPress={() => setSelected(index)}>
                <View
                  style={[
                    styles.btn,
                    {
                      backgroundColor: side.color,
                      borderWidth: selected === 0 ? 1 : 0,
                    },
                  ]}
                >
                  <Text style={{ fontSize: 17 }}>{side.side}</Text>
                </View>
              </TouchableWithoutFeedback>
            ))}
          </View>
          <View style={{ flexDirection: 'row', marginVertical: 5 }}>
            <TouchableWithoutFeedback onPress={handleSubmit}>
              <View style={[styles.btn, { backgroundColor: isUploading ? 'green' : 'black' }]}>
                <Text style={{ color: 'white', fontSize: 17 }}>
                  {isUploading ? `Uploading ${progress}` : 'SUBMIT RESPONSE'}
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    )
  }
}

export default Response

function makeId() {
  let result = ''
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < 10; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

const styles = StyleSheet.create({
  information: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#000',
    justifyContent: 'center',
  },
  cameraPreview: {
    flex: 1,
    padding: 20,
  },
  circleStyle: {
    width: 80,
    height: 80,
    borderColor: '#FFFF',
    borderRadius: 50,
    borderWidth: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    elevation: 1,
  },
  innerFakeShadow: {
    width: 70,
    height: 70,
    borderRadius: 50,
    borderColor: '#BABABA33',
    borderWidth: 1,
    borderBottomWidth: 0,
    overflow: 'hidden',
    justifyContent: 'flex-end',
  },
  btn: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 7,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    marginHorizontal: 10,
    borderColor: '#FFE1A8',
  },
  btnsContainer: {
    position: 'absolute',
    width: width,
    height: 200,
    bottom: 50,
    justifyContent: 'flex-end',
  },
  recordingContainer: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  switchCamContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  recordingBtnContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
