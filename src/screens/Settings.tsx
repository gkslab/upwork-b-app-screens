import React, { FC } from 'react'
import { SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'

const Settings: FC = () => {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Text style={styles.title}>Settings</Text>
        <View>
          <TouchableWithoutFeedback onPress={() => alert('Edit My Profile')}>
            <Text style={styles.btn}>Edit My Profile</Text>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => alert('Terms & Conditions')}>
            <Text style={styles.btn}>Terms & Conditions</Text>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => alert('Privacy Policy')}>
            <Text style={styles.btn}>Privacy Policy</Text>
          </TouchableWithoutFeedback>
        </View>
        <View>
          <TouchableWithoutFeedback onPress={() => alert('Logout')}>
            <Text style={styles.logoutBtn}>Logout</Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 20,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  logoutBtn: {
    backgroundColor: '#A8B1FF',
    textAlign: 'center',
    paddingVertical: 10,
    fontSize: 18,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  btn: {
    backgroundColor: 'white',
    paddingLeft: 7,
    paddingVertical: 10,
    fontSize: 18,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    marginVertical: 7,
  },
})

export default Settings
