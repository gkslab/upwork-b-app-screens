import { API, Auth, graphqlOperation } from 'aws-amplify'
import React, { FC } from 'react'
import { StyleSheet, View } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import UserRow from '../components/UserRow'
import { ALL_USERS } from '../graphql/customQueries'

const SearchProfile: FC = () => {
  const [users, setUsers] = React.useState<any[]>([])

  React.useEffect(() => {
    const fetchUsers = async () => {
      const { id: userID } = await Auth.currentUserInfo()
      // @ts-ignore
      const { data } = await API.graphql(graphqlOperation(ALL_USERS, { userID }))
      setUsers(data?.listUserProfiles?.items)
    }
    fetchUsers()
  }, [])

  return (
    <View style={styles.container}>
      <View style={styles.line} />
      <FlatList
        data={users}
        keyExtractor={({ id }) => id.toString()}
        renderItem={({ item }) => (
          <UserRow name={item?.name} username={item?.username} avatar={item?.avatar} />
        )}
      />
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  line: {
    borderBottomColor: '#C4C4C488',
    borderBottomWidth: 1,
    marginHorizontal: -10,
    marginBottom: 20,
    marginTop: 5,
  },
})

export default SearchProfile
