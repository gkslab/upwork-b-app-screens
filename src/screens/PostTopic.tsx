import { useNavigation } from '@react-navigation/native'
import { API, Auth, graphqlOperation, Storage } from 'aws-amplify'
import React, { FC, useState } from 'react'
import {
  Image,
  Modal,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import { CreateTopicMutation } from '../API'
import { createPost, createTopic } from '../graphql/mutations'
import PickerModal from './PickerModal'

interface PostTopicProps {
  route: any
}

const PostTopic: FC<PostTopicProps> = ({ route }) => {
  const navigation = useNavigation()
  //States that you'll need
  const [title, setTitle] = useState<string>('')
  const [description, setDescription] = useState<string>('')
  const [forText, setForText] = useState<string>('')
  const [againstText, setAgainstText] = useState<string>('')
  const [forColor, setForColor] = useState<string>('#88A')
  const [againstColor, setAgainstColor] = useState<string>('#8A8')
  const { videoPath, thumbPath } = route.params

  const [modalVisible, setModalVisible] = useState<boolean>(false)
  const [modalVisible2, setModalVisible2] = useState<boolean>(false)

  const [progress, setProgress] = React.useState('')
  const [isUploading, setUploading] = React.useState(false)

  const handleSubmit = async () => {
    try {
      setProgress('')
      setUploading(true)
      const videoResponse = await fetch(videoPath)
      const video = await videoResponse.blob()
      const videoExt = videoPath.split('.').pop()
      // @ts-ignore
      const { key: videoKey } = await Storage.put(`${makeId()}.${videoExt}`, video, {
        progressCallback(progress: any) {
          setProgress(`${Math.round((progress.loaded / progress.total) * 100)}`)
        },
      })

      const thumbnailResponse = await fetch(thumbPath)
      const thumbnail = await thumbnailResponse.blob()
      const thumbnailExt = thumbPath.split('.').pop()
      // @ts-ignore
      const { key: thumbnailKey } = await Storage.put(`${makeId()}.${thumbnailExt}`, thumbnail)
      const { id: userID } = await Auth.currentUserInfo()

      const sides = [
        { side: forText, color: forColor },
        { side: againstText, color: againstColor },
      ]

      const topicInput = {
        userID,
        name: title,
        description,
        sides,
        thumbnail: thumbnailKey,
      }

      console.log('topicInput', topicInput)

      const { data }: { data: CreateTopicMutation } = await API.graphql(
        graphqlOperation(createTopic, {
          input: topicInput,
        }),
      )
      const topicID = data?.createTopic?.id as string

      const postInput = {
        userID,
        videoUrl: videoKey,
        topicID,
        side: sides[0],
        thumbnail: thumbnailKey,
      }

      console.log('postInput', postInput)

      await API.graphql(
        graphqlOperation(createPost, {
          input: postInput,
        }),
      )

      setUploading(false)
      navigation.navigate('Home')
    } catch (error) {
      alert('Failed to upload topic')
      console.log('Failed to upload topic', error)
      setUploading(false)
    }
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Modal animationType="slide" visible={modalVisible}>
        <PickerModal
          closeModal={() => setModalVisible(false)}
          setBgColor={(color) => setForColor(color)}
        />
      </Modal>
      <Modal animationType="slide" visible={modalVisible2}>
        <PickerModal
          closeModal={() => setModalVisible2(false)}
          setBgColor={(color) => setAgainstColor(color)}
        />
      </Modal>
      <View style={styles.container}>
        <View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 3 }}>
              <View style={{ flexDirection: 'row' }}>
                <TextInput
                  onChangeText={(text) => setTitle(text)}
                  value={title}
                  style={styles.inputStyle}
                  placeholder={'Title'}
                />
              </View>
              <View style={{ flexDirection: 'row' }}>
                <TextInput
                  onChangeText={(text) => setDescription(text)}
                  value={description}
                  style={styles.inputStyle}
                  placeholder={'Descriptions & Hashtags'}
                  multiline={true}
                  numberOfLines={5}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingRight: 50,
                }}
              >
                <TextInput
                  onChangeText={(text) => setForText(text)}
                  value={forText}
                  style={styles.inputStyle}
                  placeholder={'For'}
                />
                <TouchableWithoutFeedback onPress={() => setModalVisible(true)}>
                  <View
                    style={{
                      backgroundColor: forColor,
                      width: 20,
                      height: 20,
                      borderRadius: 50,
                      marginLeft: 5,
                    }}
                  />
                </TouchableWithoutFeedback>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  paddingRight: 50,
                }}
              >
                <TextInput
                  onChangeText={(text) => setAgainstText(text)}
                  value={againstText}
                  style={styles.inputStyle}
                  placeholder={'Against'}
                />
                <TouchableWithoutFeedback onPress={() => setModalVisible2(true)}>
                  <View
                    style={{
                      backgroundColor: againstColor,
                      width: 20,
                      height: 20,
                      borderRadius: 50,
                      marginLeft: 5,
                    }}
                  />
                </TouchableWithoutFeedback>
              </View>
            </View>
            <View style={{ flex: 2, paddingLeft: 10 }}>
              <Image source={{ uri: thumbPath }} resizeMode="cover" style={styles.previewStyle} />
            </View>
          </View>
          <View style={styles.line} />
        </View>
        <View>
          <TouchableWithoutFeedback onPress={handleSubmit}>
            <Text style={styles.btn}>{isUploading ? `Uploading ${progress}` : 'POST'}</Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 20,
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  btn: {
    backgroundColor: '#A8B1FF',
    textAlign: 'center',
    paddingVertical: 10,
    fontSize: 18,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  inputStyle: {
    flex: 1,
    backgroundColor: 'white',
    fontSize: 14,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    paddingVertical: 5,
    marginVertical: 5,
    paddingLeft: 10,
  },
  previewStyle: {
    borderRadius: 10,
    marginVertical: 5,
    width: '100%',
    height: 250,
  },
  line: {
    borderBottomColor: '#C4C4C488',
    borderBottomWidth: 1,
    marginHorizontal: -10,
    marginTop: 30,
  },
})

function makeId() {
  let result = ''
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  const charactersLength = characters.length
  for (let i = 0; i < 10; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

export default PostTopic
