import { Video } from 'expo-av'
import { Camera } from 'expo-camera'
import * as Permissions from 'expo-permissions'
import * as VideoThumbnails from 'expo-video-thumbnails'
import React, { FC, useEffect, useRef, useState } from 'react'
import {
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'

const { height, width } = Dimensions.get('window')

interface StartTopicProps {
  navigation: any
}

const StartTopic: FC<StartTopicProps> = ({ navigation }) => {
  const [videoPath, setVideoPath] = useState<string>('')
  const [hasPermission, setHasPermission] = useState<any>(null)
  const videoRef = useRef<Video>(null)
  const cameraRef = useRef<Camera>(null)

  const [finished, setFinished] = useState<boolean>(false)
  const [isRecording, setIsRecording] = useState<boolean>(false)
  const [type, setType] = useState<any>(Camera.Constants.Type.front)

  const [imagePadding, setImagePadding] = useState<number>(0)
  const [ratio, setRatio] = useState<string>('4:3')
  const screenRatio = height / width
  const [isRatioSet, setIsRatioSet] = useState<boolean>(false)
  const timerRef = useRef<any>()

  useEffect(() => {
    async function getCameraStatus() {
      const statusCamera = await Permissions.askAsync(Permissions.CAMERA)
      const statusAudio = await Permissions.askAsync(Permissions.AUDIO_RECORDING)
      setHasPermission(statusCamera.status === 'granted' && statusAudio.status === 'granted')
    }
    getCameraStatus()
    return () => clearTimeout(timerRef.current)
  }, [])

  const prepareRatio = async () => {
    let desiredRatio: any = '4:3'
    if (!cameraRef.current) return
    if (Platform.OS === 'android') {
      const ratios = await cameraRef.current.getSupportedRatiosAsync()
      const distances = []
      const realRatios = []
      let minDistance = null
      for (const ratio of ratios) {
        const parts = ratio.split(':')
        const realRatio = parseInt(parts[0]) / parseInt(parts[1])
        realRatios[ratio] = realRatio
        const distance = screenRatio - realRatio
        distances[ratio] = realRatio
        if (minDistance == null) {
          minDistance = ratio
        } else {
          if (distance >= 0 && distance < distances[minDistance]) {
            minDistance = ratio
          }
        }
      }
      desiredRatio = minDistance
      const remainder = Math.floor((height - realRatios[desiredRatio] * width) / 2)
      setImagePadding(remainder / 2)
      setRatio(desiredRatio)
      setIsRatioSet(true)
    }
  }

  const setCameraReady = async () => {
    if (!isRatioSet) {
      await prepareRatio()
    }
  }

  const recordVideo = async () => {
    if (!cameraRef.current || !cameraRef.current) return
    setFinished(false)
    setIsRecording(true)
    const timer = setTimeout(function () {
      if (!finished) stopRecording()
    }, 30000)
    timerRef.current = timer
    const data = await cameraRef.current.recordAsync()
    if (data && videoRef.current) {
      await videoRef.current.loadAsync({ uri: data.uri })
      await videoRef.current.playAsync()
      await videoRef.current.setIsLoopingAsync(true)
      setVideoPath(data.uri)
    }
    setIsRecording(false)
  }
  const stopRecording = async () => {
    if (!finished && cameraRef.current) {
      await cameraRef.current.stopRecording()
      setFinished(true)
    }
  }
  const stopAndNavigate = async () => {
    if (!videoRef.current) return
    await videoRef.current.stopAsync()
    const { uri: thumbPath } = await VideoThumbnails.getThumbnailAsync(videoPath, {
      time: 15,
    })
    navigation.navigate('PostTopic', { videoPath, thumbPath })
  }

  if (hasPermission === null) {
    return (
      <View style={styles.information}>
        <Text>Waiting for camera permissions</Text>
      </View>
    )
  } else if (hasPermission === false) {
    return (
      <View style={styles.information}>
        <Text>No access to camera</Text>
      </View>
    )
  } else {
    return (
      <View style={styles.container}>
        {finished ? (
          <Video
            rate={1.0}
            volume={1.0}
            isMuted={false}
            resizeMode="cover"
            shouldPlay
            isLooping={true}
            style={[
              styles.cameraPreview,
              {
                marginTop: imagePadding,
                marginBottom: imagePadding,
              },
            ]}
            ref={videoRef}
          />
        ) : (
          <Camera
            type={type}
            style={[styles.cameraPreview, { marginTop: imagePadding, marginBottom: imagePadding }]}
            onCameraReady={setCameraReady}
            ratio={ratio}
            ref={cameraRef}
          />
        )}
        <View style={{ position: 'absolute', top: 20, right: 20 }}>
          <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
            <Text style={{ fontSize: 28 }}>X</Text>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.btnsContainer}>
          {finished ? null : (
            <View style={styles.recordingContainer}>
              <View style={styles.switchCamContainer}>
                <TouchableWithoutFeedback
                  onPress={() =>
                    setType(
                      type === Camera.Constants.Type.front
                        ? Camera.Constants.Type.back
                        : Camera.Constants.Type.front,
                    )
                  }
                >
                  <Image
                    source={require('../assets/cameraSwitch.png')}
                    style={{ width: 47, height: 38 }}
                  />
                </TouchableWithoutFeedback>
              </View>
              <View style={styles.recordingBtnContainer}>
                <TouchableWithoutFeedback
                  onLongPress={() => recordVideo()}
                  onPressOut={() => stopRecording()}
                >
                  <View
                    style={[
                      styles.circleStyle,
                      {
                        backgroundColor: isRecording ? '#FF5555' : '#FFFFFF44',
                      },
                    ]}
                  >
                    <View style={styles.innerFakeShadow}></View>
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={{ flex: 1 }} />
            </View>
          )}

          <View style={{ flexDirection: 'row', marginVertical: 5 }}>
            <TouchableWithoutFeedback onPress={() => stopAndNavigate()}>
              <View style={[styles.btn, { backgroundColor: 'black' }]}>
                <Text style={{ color: 'white', fontSize: 17 }}>PROCEED</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  information: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#000',
    justifyContent: 'center',
  },
  cameraPreview: {
    flex: 1,
    padding: 20,
  },
  circleStyle: {
    width: 80,
    height: 80,
    borderColor: '#FFFF',
    borderRadius: 50,
    borderWidth: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    elevation: 1,
  },
  innerFakeShadow: {
    width: 70,
    height: 70,
    borderRadius: 50,
    borderColor: '#BABABA33',
    borderWidth: 1,
    borderBottomWidth: 0,
    overflow: 'hidden',
    justifyContent: 'flex-end',
  },
  btn: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 7,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    marginHorizontal: 10,
    borderColor: '#FFE1A8',
  },
  btnsContainer: {
    position: 'absolute',
    width: width,
    height: 200,
    bottom: 50,
    justifyContent: 'flex-end',
  },
  recordingContainer: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  switchCamContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  recordingBtnContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default StartTopic
