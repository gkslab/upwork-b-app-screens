import React, { FC } from 'react'
import { Text, TouchableWithoutFeedback, View } from 'react-native'
import { ColorPicker } from 'react-native-color-picker'
interface PickerModalProps {
  closeModal: () => void
  setBgColor: (color: string) => void
}

const PickerModal: FC<PickerModalProps> = ({ closeModal, setBgColor }) => {
  return (
    <View style={{ flex: 1, paddingVertical: 30 }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          marginVertical: 40,
        }}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            closeModal()
          }}
        >
          <Text style={{ fontSize: 20 }}>CLOSE</Text>
        </TouchableWithoutFeedback>
      </View>
      <ColorPicker
        onColorSelected={(color) => {
          setBgColor(color)
          closeModal()
        }}
        style={{ flex: 1 }}
      />
    </View>
  )
}
export default PickerModal
