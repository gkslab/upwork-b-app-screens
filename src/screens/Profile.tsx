import { useIsFocused } from '@react-navigation/native'
import { API, Auth, graphqlOperation } from 'aws-amplify'
import React, { FC, useState } from 'react'
import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'
import ProfilePost from '../components/ProfilePost'
import ProfileReaction from '../components/ProfileReaction'
import { MY_REACTIONS, MY_TOPICS } from '../graphql/customQueries'

const Profile: FC = () => {
  const [selectedMode, setSelectedMode] = useState<string>('posts')
  const [topics, setTopics] = React.useState<any[]>([])
  const [reactions, setReactions] = React.useState<any[]>([])
  const [userInfo, setUserInfo] = React.useState()
  const isFocused = useIsFocused()

  React.useEffect(() => {
    const fetchUserInfo = async () => {
      const info = await Auth.currentUserInfo()
      setUserInfo(info)
    }
    fetchUserInfo()
  }, [])

  React.useEffect(() => {
    const fetchData = async () => {
      if (!userInfo) return
      // @ts-ignore
      const { data: topicData } = await API.graphql(
        graphqlOperation(MY_TOPICS, {
          userID: userInfo.id,
        }),
      )
      setTopics(topicData?.listTopics?.items)

      // @ts-ignore
      const { data: responseData } = await API.graphql(
        graphqlOperation(MY_REACTIONS, {
          userID: userInfo.id,
        }),
      )
      setReactions(responseData?.listPosts?.items)
    }
    if (isFocused) {
      fetchData()
    }
  }, [isFocused, userInfo])

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <View style={styles.row}>
            <View style={styles.imgPlaceholder} />
            <View>
              <Text style={styles.name}>A mighty nickname</Text>
              <Text style={styles.name}>{userInfo?.username}</Text>
              <Text style={styles.bio}>{userInfo?.attributes?.email}</Text>
            </View>
          </View>
          <TouchableWithoutFeedback onPress={() => alert('setting')}>
            <View style={{ alignItems: 'center' }}>
              <Image
                resizeMode={'contain'}
                source={require('../assets/settings.png')}
                style={{ width: 25, height: 30 }}
              />
              <Text style={styles.label}>Settings</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 50 }}>
          <TouchableWithoutFeedback onPress={() => setSelectedMode('posts')}>
            <View
              style={[
                styles.tabItem,
                {
                  backgroundColor: selectedMode === 'posts' ? '#FFE1A8' : 'transparent',
                },
              ]}
            >
              <Text style={styles.tabText}>My Posts</Text>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => setSelectedMode('reactions')}>
            <View
              style={[
                styles.tabItem,
                {
                  backgroundColor: selectedMode === 'reactions' ? '#FFE1A8' : 'transparent',
                },
              ]}
            >
              <Text style={styles.tabText}>My Reactions</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.line} />

        {selectedMode === 'posts' ? (
          <FlatList
            data={topics}
            keyExtractor={({ id }) => '_' + id.toString()}
            renderItem={({ item }) => <ProfilePost item={item} />}
            numColumns={3}
            style={{ marginHorizontal: -10 }}
          />
        ) : (
          <FlatList
            data={reactions}
            keyExtractor={({ id }) => '#' + id.toString()}
            renderItem={({ item }) => <ProfileReaction item={item} />}
            numColumns={3}
            style={{ marginHorizontal: -10 }}
          />
        )}
      </View>
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 20,
    backgroundColor: 'white',
  },
  row: { flexDirection: 'row', alignItems: 'center', margin: 5 },
  imgPlaceholder: {
    backgroundColor: '#C4C4C4',
    width: 70,
    height: 70,
    borderRadius: 50,
    marginRight: 10,
  },
  name: {
    fontSize: 15,
    lineHeight: 19,
  },
  bio: {
    fontSize: 15,
    color: '#C4C4C4',
    lineHeight: 19,
  },
  label: {
    fontSize: 12,
    color: '#C4C4C4',
  },
  tabItem: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    borderRadius: 10,
  },
  tabText: {
    flex: 1,
    textAlign: 'center',
    paddingVertical: 5,
  },
  line: {
    borderBottomColor: '#C4C4C488',
    borderBottomWidth: 1,
    marginHorizontal: -10,
    marginBottom: 20,
    marginTop: 5,
  },
})

export default Profile
