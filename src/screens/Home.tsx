import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native'
import { API, Auth, graphqlOperation, Storage } from 'aws-amplify'
import React, { FC } from 'react'
import { Dimensions, Image, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import GestureRecognizer from 'react-native-swipe-gestures'
import Playlist from '../components/Playlist'
import {
  HOME_QUERY,
  MY_REACTIONS_DETAIL,
  MY_TOPICS_DETAIL,
  PRIVATE_TOPICS_DETAIL,
  PUBLIC_TOPICS_DETAIL,
} from '../graphql/customQueries'

const Post: React.FC<{ user?: any; thumbnailKey?: string | null; side?: any }> = (props) => {
  const { user, thumbnailKey, side } = props
  const [thumbnail, setThumbnail] = React.useState<string>()

  React.useEffect(() => {
    const fetchThumb = async () => {
      if (!thumbnailKey) return
      const uri = (await Storage.get(thumbnailKey)) as string
      setThumbnail(uri)
    }
    fetchThumb()
  }, [thumbnailKey])

  return (
    <View style={[styles.rowContainer]}>
      <View style={{ flexDirection: 'row' }}>
        <Image source={{ uri: thumbnail }} style={styles.rectStyle} />
        <View style={styles.whiteCircle} />
        <Text style={styles.usernameStyle}>{user?.name}</Text>
      </View>
      <View style={[styles.secondBtn, { backgroundColor: side?.color }]}>
        <Text style={{ color: 'white', textAlign: 'center', fontSize: 17 }}>{side?.side}</Text>
      </View>
    </View>
  )
}

interface HomeProps {
  navigation: any
}

const { width } = Dimensions.get('window')
const screenWidth = width

type Topic = {
  name: string
  posts: {
    items: Array<{
      user: {
        name: string
      } | null
      side: {
        color: string
        side: string
      } | null
      thumbnail: string | null
      id: string
      videoUrl: string
    } | null> | null
  } | null
}

const Home: FC<HomeProps> = () => {
  const navigation = useNavigation()
  const { params } = useRoute()
  const isFocused = useIsFocused()
  const config = {
    velocityThreshold: 0.3,
    directionalOffsetThreshold: 80,
  }

  // Topics state
  const [topics, setTopics] = React.useState<any[]>([])
  const [index, setIndex] = React.useState(0)
  const [nextToken, setNextToken] = React.useState<string>()
  const [isRefreshing, setRefreshing] = React.useState(false)

  const onTopicsEndReached = () => alert('You reached the end.')
  const setNextTopic = () =>
    setIndex((prev) => {
      if (prev === topics.length - 1) {
        onTopicsEndReached()
        return topics.length - 1
      } else {
        return prev + 1
      }
    })
  const setPrevTopic = () =>
    setIndex((prev) => {
      if (prev === 0) {
        onTopicsEndReached()
        return 0
      } else {
        return prev - 1
      }
    })

  const topic = topics[index] as Topic
  const posts = React.useMemo(() => topic?.posts?.items || [], [topic?.posts?.items])
  const videoKeys = posts.reduce((keys: string[], post) => {
    if (post?.videoUrl) {
      keys = [...keys, post.videoUrl]
    }
    return keys
  }, [])

  const fetchTopics = async () => {
    setRefreshing(true)
    // Different fetching logic if navigated from other screens
    if (params && params.topicID && params.prevScreen) {
      const { id: userID } = await Auth.currentUserInfo()

      let query = HOME_QUERY
      switch (params.prevScreen) {
        case 'publicPosts':
          query = PUBLIC_TOPICS_DETAIL
          break
        case 'privatePosts':
          query = PRIVATE_TOPICS_DETAIL
          break

        case 'myTopics':
          query = MY_TOPICS_DETAIL
          break

        case 'myReactions':
          query = MY_REACTIONS_DETAIL
          break

        default:
          query = HOME_QUERY
          break
      }

      let variables = undefined
      switch (params.prevScreen) {
        case 'myTopics':
          variables = { userID }
          break

        case 'myReactions':
          variables = { userID }
          break

        default:
          variables = undefined
          break
      }

      const { data } = await API.graphql(graphqlOperation(query, variables))
      setNextToken(data?.listTopics?.nextToken)
      let items = data?.listTopics?.items as any[]
      const viewedTopic = items.find((item) => item.id === params.topicID)
      if (viewedTopic) {
        // move the topic from params to the first place in items array
        items = items.filter((item) => item.id !== params.topicID)
        items.unshift(viewedTopic)
      }
      setTopics(items)
      setRefreshing(false)
      return
    }

    const { data } = await API.graphql(graphqlOperation(HOME_QUERY))
    setTopics(data?.listTopics?.items)
    setNextToken(data?.listTopics?.nextToken)
    setRefreshing(false)
  }

  React.useEffect(() => {
    if (isFocused) {
      fetchTopics()
    }
  }, [isFocused])

  return (
    <View style={styles.container}>
      <GestureRecognizer
        // onSwipeUp={() => alert('onSwipeUp')}
        // onSwipeDown={() => alert('onSwipeDown')}
        onSwipeLeft={setNextTopic}
        onSwipeRight={setPrevTopic}
        config={config}
      >
        <View style={styles.imgStyle}>
          <Playlist keys={videoKeys} shouldPlay={isFocused} />
          <View style={styles.row}>
            <View style={{ flex: 3 }}>
              <Text style={styles.title}>{topic?.name}</Text>
            </View>
            <View style={{ flex: 2, alignItems: 'flex-end' }}>
              <TouchableWithoutFeedback onPress={() => alert('user')}>
                <View style={styles.iconContainer}>
                  <Image
                    style={styles.profilePicStyle}
                    source={{
                      uri: 'https://media.salon.com/2012/06/obama_immigration_square.jpg',
                    }}
                  />
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => alert('like')}>
                <View style={styles.iconContainer}>
                  <Image style={{ width: 30, height: 30 }} source={require('../assets/like.png')} />
                  <Text style={styles.label}>1.1k</Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => alert('share')}>
                <View style={styles.iconContainer}>
                  <Image
                    style={{ width: 30, height: 30 }}
                    source={require('../assets/share.png')}
                  />
                  <Text style={styles.label}>Share</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </View>
      </GestureRecognizer>
      <TouchableWithoutFeedback
        onPress={() => navigation.navigate('Response', { topicID: topic.id, sides: topic.sides })}
      >
        <View style={styles.btn}>
          <Text style={styles.btnText}>MY RESPONSE</Text>
          <Image style={{ width: 35, height: 35 }} source={require('../assets/video.png')} />
        </View>
      </TouchableWithoutFeedback>
      <FlatList
        data={posts}
        keyExtractor={(item) => item.id}
        onRefresh={fetchTopics}
        refreshing={isRefreshing}
        renderItem={({ item }) => <Post {...item} thumbnailKey={item?.thumbnail} />}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    paddingBottom: 5,
  },
  imgStyle: {
    width: screenWidth,
    height: 500,
    justifyContent: 'flex-end',
    padding: 5,
  },
  profilePicStyle: {
    width: 30,
    height: 30,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#EA66FF',
  },
  iconContainer: { marginVertical: 5, alignItems: 'center' },
  label: {
    color: 'white',
    fontSize: 12,
  },
  btn: {
    flexDirection: 'row',
    backgroundColor: '#A8B1FF',
    borderRadius: 10,
    paddingVertical: 3,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginHorizontal: 5,
  },
  btnText: { fontSize: 16, marginRight: 5 },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 5,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 10,
    marginVertical: 10,
  },
  rectStyle: {
    backgroundColor: '#C4C4C4',
    width: 82,
    height: 66,
    margin: 5,
    borderRadius: 10,
  },
  whiteCircle: {
    backgroundColor: 'white',
    width: 22,
    height: 22,
    borderRadius: 60,
    alignSelf: 'center',
    marginHorizontal: 10,
  },
  secondBtn: {
    width: 80,
    justifyContent: 'center',
    paddingHorizontal: 5,
    borderRadius: 10,
    margin: -1,
  },
  usernameStyle: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 12,
  },
})

export default Home
