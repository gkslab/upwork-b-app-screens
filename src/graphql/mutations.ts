/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUserProfile = /* GraphQL */ `
  mutation CreateUserProfile(
    $input: CreateUserProfileInput!
    $condition: ModelUserProfileConditionInput
  ) {
    createUserProfile(input: $input, condition: $condition) {
      id
      username
      private
      name
      dateOfBirth
      avatar
      description
      totalFollowing
      totalReactions
      totalPosts
      topics {
        nextToken
      }
      followRequests {
        nextToken
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const updateUserProfile = /* GraphQL */ `
  mutation UpdateUserProfile(
    $input: UpdateUserProfileInput!
    $condition: ModelUserProfileConditionInput
  ) {
    updateUserProfile(input: $input, condition: $condition) {
      id
      username
      private
      name
      dateOfBirth
      avatar
      description
      totalFollowing
      totalReactions
      totalPosts
      topics {
        nextToken
      }
      followRequests {
        nextToken
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const deleteUserProfile = /* GraphQL */ `
  mutation DeleteUserProfile(
    $input: DeleteUserProfileInput!
    $condition: ModelUserProfileConditionInput
  ) {
    deleteUserProfile(input: $input, condition: $condition) {
      id
      username
      private
      name
      dateOfBirth
      avatar
      description
      totalFollowing
      totalReactions
      totalPosts
      topics {
        nextToken
      }
      followRequests {
        nextToken
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const createFollowers = /* GraphQL */ `
  mutation CreateFollowers(
    $input: CreateFollowersInput!
    $condition: ModelFollowersConditionInput
  ) {
    createFollowers(input: $input, condition: $condition) {
      id
      userID
      requestor
      approved
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedAt
      owner
    }
  }
`;
export const updateFollowers = /* GraphQL */ `
  mutation UpdateFollowers(
    $input: UpdateFollowersInput!
    $condition: ModelFollowersConditionInput
  ) {
    updateFollowers(input: $input, condition: $condition) {
      id
      userID
      requestor
      approved
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedAt
      owner
    }
  }
`;
export const deleteFollowers = /* GraphQL */ `
  mutation DeleteFollowers(
    $input: DeleteFollowersInput!
    $condition: ModelFollowersConditionInput
  ) {
    deleteFollowers(input: $input, condition: $condition) {
      id
      userID
      requestor
      approved
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedAt
      owner
    }
  }
`;
export const createTopic = /* GraphQL */ `
  mutation CreateTopic(
    $input: CreateTopicInput!
    $condition: ModelTopicConditionInput
  ) {
    createTopic(input: $input, condition: $condition) {
      id
      userID
      name
      description
      sides {
        side
        color
      }
      likes
      thumbnail
      posts {
        nextToken
      }
      tags
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const updateTopic = /* GraphQL */ `
  mutation UpdateTopic(
    $input: UpdateTopicInput!
    $condition: ModelTopicConditionInput
  ) {
    updateTopic(input: $input, condition: $condition) {
      id
      userID
      name
      description
      sides {
        side
        color
      }
      likes
      thumbnail
      posts {
        nextToken
      }
      tags
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const deleteTopic = /* GraphQL */ `
  mutation DeleteTopic(
    $input: DeleteTopicInput!
    $condition: ModelTopicConditionInput
  ) {
    deleteTopic(input: $input, condition: $condition) {
      id
      userID
      name
      description
      sides {
        side
        color
      }
      likes
      thumbnail
      posts {
        nextToken
      }
      tags
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const createPost = /* GraphQL */ `
  mutation CreatePost(
    $input: CreatePostInput!
    $condition: ModelPostConditionInput
  ) {
    createPost(input: $input, condition: $condition) {
      id
      userID
      topicID
      videoUrl
      content
      flag
      likes
      thumbnail
      side {
        side
        color
      }
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      topic {
        id
        userID
        name
        description
        likes
        thumbnail
        tags
        createdOn
        updatedOn
        owner
      }
      comments {
        nextToken
      }
      responses {
        nextToken
      }
      tags
      createdOn
      updatedOn
      owner
    }
  }
`;
export const updatePost = /* GraphQL */ `
  mutation UpdatePost(
    $input: UpdatePostInput!
    $condition: ModelPostConditionInput
  ) {
    updatePost(input: $input, condition: $condition) {
      id
      userID
      topicID
      videoUrl
      content
      flag
      likes
      thumbnail
      side {
        side
        color
      }
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      topic {
        id
        userID
        name
        description
        likes
        thumbnail
        tags
        createdOn
        updatedOn
        owner
      }
      comments {
        nextToken
      }
      responses {
        nextToken
      }
      tags
      createdOn
      updatedOn
      owner
    }
  }
`;
export const deletePost = /* GraphQL */ `
  mutation DeletePost(
    $input: DeletePostInput!
    $condition: ModelPostConditionInput
  ) {
    deletePost(input: $input, condition: $condition) {
      id
      userID
      topicID
      videoUrl
      content
      flag
      likes
      thumbnail
      side {
        side
        color
      }
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      topic {
        id
        userID
        name
        description
        likes
        thumbnail
        tags
        createdOn
        updatedOn
        owner
      }
      comments {
        nextToken
      }
      responses {
        nextToken
      }
      tags
      createdOn
      updatedOn
      owner
    }
  }
`;
export const createComment = /* GraphQL */ `
  mutation CreateComment(
    $input: CreateCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    createComment(input: $input, condition: $condition) {
      id
      content
      postID
      updatedAt
      createdAt
      owner
    }
  }
`;
export const updateComment = /* GraphQL */ `
  mutation UpdateComment(
    $input: UpdateCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    updateComment(input: $input, condition: $condition) {
      id
      content
      postID
      updatedAt
      createdAt
      owner
    }
  }
`;
export const deleteComment = /* GraphQL */ `
  mutation DeleteComment(
    $input: DeleteCommentInput!
    $condition: ModelCommentConditionInput
  ) {
    deleteComment(input: $input, condition: $condition) {
      id
      content
      postID
      updatedAt
      createdAt
      owner
    }
  }
`;
export const createResponse = /* GraphQL */ `
  mutation CreateResponse(
    $input: CreateResponseInput!
    $condition: ModelResponseConditionInput
  ) {
    createResponse(input: $input, condition: $condition) {
      id
      postID
      responseCode
      updatedAt
      createdAt
      owner
    }
  }
`;
export const updateResponse = /* GraphQL */ `
  mutation UpdateResponse(
    $input: UpdateResponseInput!
    $condition: ModelResponseConditionInput
  ) {
    updateResponse(input: $input, condition: $condition) {
      id
      postID
      responseCode
      updatedAt
      createdAt
      owner
    }
  }
`;
export const deleteResponse = /* GraphQL */ `
  mutation DeleteResponse(
    $input: DeleteResponseInput!
    $condition: ModelResponseConditionInput
  ) {
    deleteResponse(input: $input, condition: $condition) {
      id
      postID
      responseCode
      updatedAt
      createdAt
      owner
    }
  }
`;
