/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateUserProfile = /* GraphQL */ `
  subscription OnCreateUserProfile {
    onCreateUserProfile {
      id
      username
      private
      name
      dateOfBirth
      avatar
      description
      totalFollowing
      totalReactions
      totalPosts
      topics {
        nextToken
      }
      followRequests {
        nextToken
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onUpdateUserProfile = /* GraphQL */ `
  subscription OnUpdateUserProfile {
    onUpdateUserProfile {
      id
      username
      private
      name
      dateOfBirth
      avatar
      description
      totalFollowing
      totalReactions
      totalPosts
      topics {
        nextToken
      }
      followRequests {
        nextToken
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onDeleteUserProfile = /* GraphQL */ `
  subscription OnDeleteUserProfile {
    onDeleteUserProfile {
      id
      username
      private
      name
      dateOfBirth
      avatar
      description
      totalFollowing
      totalReactions
      totalPosts
      topics {
        nextToken
      }
      followRequests {
        nextToken
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onCreateFollowers = /* GraphQL */ `
  subscription OnCreateFollowers {
    onCreateFollowers {
      id
      userID
      requestor
      approved
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedAt
      owner
    }
  }
`;
export const onUpdateFollowers = /* GraphQL */ `
  subscription OnUpdateFollowers {
    onUpdateFollowers {
      id
      userID
      requestor
      approved
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedAt
      owner
    }
  }
`;
export const onDeleteFollowers = /* GraphQL */ `
  subscription OnDeleteFollowers {
    onDeleteFollowers {
      id
      userID
      requestor
      approved
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedAt
      owner
    }
  }
`;
export const onCreateTopic = /* GraphQL */ `
  subscription OnCreateTopic {
    onCreateTopic {
      id
      userID
      name
      description
      sides {
        side
        color
      }
      likes
      thumbnail
      posts {
        nextToken
      }
      tags
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onUpdateTopic = /* GraphQL */ `
  subscription OnUpdateTopic {
    onUpdateTopic {
      id
      userID
      name
      description
      sides {
        side
        color
      }
      likes
      thumbnail
      posts {
        nextToken
      }
      tags
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onDeleteTopic = /* GraphQL */ `
  subscription OnDeleteTopic {
    onDeleteTopic {
      id
      userID
      name
      description
      sides {
        side
        color
      }
      likes
      thumbnail
      posts {
        nextToken
      }
      tags
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onCreatePost = /* GraphQL */ `
  subscription OnCreatePost {
    onCreatePost {
      id
      userID
      topicID
      videoUrl
      content
      flag
      likes
      thumbnail
      side {
        side
        color
      }
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      topic {
        id
        userID
        name
        description
        likes
        thumbnail
        tags
        createdOn
        updatedOn
        owner
      }
      comments {
        nextToken
      }
      responses {
        nextToken
      }
      tags
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onUpdatePost = /* GraphQL */ `
  subscription OnUpdatePost {
    onUpdatePost {
      id
      userID
      topicID
      videoUrl
      content
      flag
      likes
      thumbnail
      side {
        side
        color
      }
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      topic {
        id
        userID
        name
        description
        likes
        thumbnail
        tags
        createdOn
        updatedOn
        owner
      }
      comments {
        nextToken
      }
      responses {
        nextToken
      }
      tags
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onDeletePost = /* GraphQL */ `
  subscription OnDeletePost {
    onDeletePost {
      id
      userID
      topicID
      videoUrl
      content
      flag
      likes
      thumbnail
      side {
        side
        color
      }
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      topic {
        id
        userID
        name
        description
        likes
        thumbnail
        tags
        createdOn
        updatedOn
        owner
      }
      comments {
        nextToken
      }
      responses {
        nextToken
      }
      tags
      createdOn
      updatedOn
      owner
    }
  }
`;
export const onCreateComment = /* GraphQL */ `
  subscription OnCreateComment {
    onCreateComment {
      id
      content
      postID
      updatedAt
      createdAt
      owner
    }
  }
`;
export const onUpdateComment = /* GraphQL */ `
  subscription OnUpdateComment {
    onUpdateComment {
      id
      content
      postID
      updatedAt
      createdAt
      owner
    }
  }
`;
export const onDeleteComment = /* GraphQL */ `
  subscription OnDeleteComment {
    onDeleteComment {
      id
      content
      postID
      updatedAt
      createdAt
      owner
    }
  }
`;
export const onCreateResponse = /* GraphQL */ `
  subscription OnCreateResponse {
    onCreateResponse {
      id
      postID
      responseCode
      updatedAt
      createdAt
      owner
    }
  }
`;
export const onUpdateResponse = /* GraphQL */ `
  subscription OnUpdateResponse {
    onUpdateResponse {
      id
      postID
      responseCode
      updatedAt
      createdAt
      owner
    }
  }
`;
export const onDeleteResponse = /* GraphQL */ `
  subscription OnDeleteResponse {
    onDeleteResponse {
      id
      postID
      responseCode
      updatedAt
      createdAt
      owner
    }
  }
`;
