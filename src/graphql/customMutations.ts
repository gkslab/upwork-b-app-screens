// /* tslint:disable */
// /* eslint-disable */

// export const createTopic = /* GraphQL */ `
//   mutation CreateTopic($input: CreateTopicInput!, $condition: ModelTopicConditionInput) {
//     createTopic(input: $input, condition: $condition) {
//       id
//       userID
//       name
//       description
//       sides {
//         side
//         color
//       }
//     }
//   }
// `
