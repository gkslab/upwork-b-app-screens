export const HOME_QUERY = `
query Home {
    listTopics(limit: 100) {
      nextToken
      items {
        id
        name
        sides {
          color
          side
        }
        posts(limit: 50, sortDirection: ASC) {
          nextToken
          items {
            user {
              name
            }
            side {
              color
              side
            }
            thumbnail
            id
            videoUrl
          }
        }
      }
    }
  }
`

export const PUBLIC_TOPICS_DETAIL = `
query PublicTopicsDetail {
    listTopics(limit: 100) {
      nextToken
      items {
        id
        name
        sides {
          color
          side
        }
        posts(limit: 50, sortDirection: ASC) {
          nextToken
          items {
            user {
              name
            }
            side {
              color
              side
            }
            thumbnail
            id
            videoUrl
          }
        }
      }
    }
  }
`
export const PRIVATE_TOPICS_DETAIL = `
query PrivateTopicsDetail {
    listTopics(limit: 100) {
      nextToken
      items {
        id
        name
        sides {
          color
          side
        }
        posts(limit: 50, sortDirection: ASC) {
          nextToken
          items {
            user {
              name
            }
            side {
              color
              side
            }
            thumbnail
            id
            videoUrl
          }
        }
      }
    }
  }
`

export const MY_TOPICS_DETAIL = `
query MyTopicsDetail($userID: ID!) {
  listTopics(limit: 100, filter: {userID: {eq: $userID}}) {
    nextToken
    items {
      id
      name
      sides {
        color
        side
      }
      posts(limit: 50, sortDirection: ASC) {
        items {
          user {
            name
          }
          side {
            color
            side
          }
          thumbnail
          id
          videoUrl
        }
      }
    }
  }
}

`

export const MY_REACTIONS_DETAIL = `
query MyTopicsDetail($userID: ID!) {
  listTopics(limit: 100, filter: {userID: {eq: $userID}}) {
    nextToken
    items {
      id
      name
      sides {
        color
        side
      }
      posts(limit: 50, sortDirection: ASC) {
        items {
          user {
            name
          }
          side {
            color
            side
          }
          thumbnail
          id
          videoUrl
        }
      }
    }
  }
}
`

export const PUBLIC_TOPICS = `
query MyTopics {
  listTopics(limit: 100) {
    items {
      id
      name
      sides {
        side
        color
      }
      posts(limit: 1, sortDirection: DESC) {
        items {
          thumbnail
        }
      }
    }
  }
}`

export const POSTS = `
query Posts {
  listPosts(limit: 50) {
    items {
      topic {
        name
        sides {
          color
          side
        }
      }
      user {
        name
      }
      side {
        color
        side
      }
      thumbnail
      id
      videoUrl
    }
  }
}
`

export const MY_REACTIONS = `
query MyResponses($userID: ID!) {
  listPosts(limit: 100, filter: {userID: {eq: $userID}}) {
    items {
      id
      thumbnail
      side {
        side
        color
      }
      topic {
        name
      }
    }
  }
}`

export const HOME_TOPIC_PAGINATION = `
query HomeTopicPagination($nextToken: String!) {
  listTopics(limit: 1, nextToken: $nextToken) {
    nextToken
    items {
      name
      description
      posts(limit: 50, sortDirection: DESC) {
        nextToken
        items {
          user {
            name
          }
          side {
            color
            side
          }
          thumbnail
          id
          videoUrl
        }
      }
    }
  }
}`

export const MY_TOPICS = `
query MyTopics($userID: ID!) {
  listTopics(limit: 100, filter: {userID: {eq: $userID}}) {
    items {
      id
      name
      sides {
        side
        color
      }
      posts(filter: {userID: {eq: $userID}}, sortDirection: DESC, limit: 1) {
        items {
          thumbnail
        }
      }
    }
  }
}`

export const ALL_USERS = `
query AllUsers($userID: ID!) {
  listUserProfiles(limit: 100, filter: {id: {ne: $userID}}) {
    items {
      avatar
      id
      name
      username
    }
  }
}`
