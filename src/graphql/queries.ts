/* tslint:disable */
/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getUserProfile = /* GraphQL */ `
  query GetUserProfile($id: ID!) {
    getUserProfile(id: $id) {
      id
      username
      private
      name
      dateOfBirth
      avatar
      description
      totalFollowing
      totalReactions
      totalPosts
      topics {
        nextToken
      }
      followRequests {
        nextToken
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const listUserProfiles = /* GraphQL */ `
  query ListUserProfiles(
    $filter: ModelUserProfileFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUserProfiles(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      nextToken
    }
  }
`;
export const getFollowers = /* GraphQL */ `
  query GetFollowers($id: ID!) {
    getFollowers(id: $id) {
      id
      userID
      requestor
      approved
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedAt
      owner
    }
  }
`;
export const listFollowerss = /* GraphQL */ `
  query ListFollowerss(
    $filter: ModelFollowersFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listFollowerss(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userID
        requestor
        approved
        createdOn
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getTopic = /* GraphQL */ `
  query GetTopic($id: ID!) {
    getTopic(id: $id) {
      id
      userID
      name
      description
      sides {
        side
        color
      }
      likes
      thumbnail
      posts {
        nextToken
      }
      tags
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      createdOn
      updatedOn
      owner
    }
  }
`;
export const listTopics = /* GraphQL */ `
  query ListTopics(
    $filter: ModelTopicFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTopics(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userID
        name
        description
        likes
        thumbnail
        tags
        createdOn
        updatedOn
        owner
      }
      nextToken
    }
  }
`;
export const getPost = /* GraphQL */ `
  query GetPost($id: ID!) {
    getPost(id: $id) {
      id
      userID
      topicID
      videoUrl
      content
      flag
      likes
      thumbnail
      side {
        side
        color
      }
      user {
        id
        username
        private
        name
        dateOfBirth
        avatar
        description
        totalFollowing
        totalReactions
        totalPosts
        createdOn
        updatedOn
        owner
      }
      topic {
        id
        userID
        name
        description
        likes
        thumbnail
        tags
        createdOn
        updatedOn
        owner
      }
      comments {
        nextToken
      }
      responses {
        nextToken
      }
      tags
      createdOn
      updatedOn
      owner
    }
  }
`;
export const listPosts = /* GraphQL */ `
  query ListPosts(
    $filter: ModelPostFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listPosts(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        userID
        topicID
        videoUrl
        content
        flag
        likes
        thumbnail
        tags
        createdOn
        updatedOn
        owner
      }
      nextToken
    }
  }
`;
export const getComment = /* GraphQL */ `
  query GetComment($id: ID!) {
    getComment(id: $id) {
      id
      content
      postID
      updatedAt
      createdAt
      owner
    }
  }
`;
export const listComments = /* GraphQL */ `
  query ListComments(
    $filter: ModelCommentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        content
        postID
        updatedAt
        createdAt
        owner
      }
      nextToken
    }
  }
`;
export const getResponse = /* GraphQL */ `
  query GetResponse($id: ID!) {
    getResponse(id: $id) {
      id
      postID
      responseCode
      updatedAt
      createdAt
      owner
    }
  }
`;
export const listResponses = /* GraphQL */ `
  query ListResponses(
    $filter: ModelResponseFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listResponses(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        postID
        responseCode
        updatedAt
        createdAt
        owner
      }
      nextToken
    }
  }
`;
