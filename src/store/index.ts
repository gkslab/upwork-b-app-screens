import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit'
import { postsSlice } from './posts.slice'

export const store = configureStore({
  reducer: {
    posts: postsSlice.reducer,
  },
})

export type AppRootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
export type AppThunk = ThunkAction<void, AppRootState, unknown, Action<string>>
