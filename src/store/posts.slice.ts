import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch, AppRootState } from '.'
import { loadPostsAsync } from './posts.thunk'

export interface Post {
  title: string
  imageSource: string
  firstTextNumber: string
  firstText: string
  firstBgColor: string
  secondTextNumber: string
  secondText: string
  secondBgColor: string
}

type PostsActions = {
  createPost: (post: Post) => void
  loadPostsAsync: () => void
}

type PostsState = {
  data: Post[]
  isLoading: boolean
  hasError: boolean
  error: string | null
}

const initialState: PostsState = {
  data: [],
  isLoading: true,
  hasError: false,
  error: null,
}

export const postsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {
    createPost(state, action: PayloadAction<Post>) {
      state.data.push(action.payload)
      state.isLoading = false
    },
  },
  extraReducers: {
    [loadPostsAsync.pending.type]: (state) => {
      state.isLoading = true
      state.hasError = false
      state.error = null
    },
    [loadPostsAsync.fulfilled.type]: (state, { payload }: PayloadAction<Post[]>) => {
      state.data = payload
      state.isLoading = false
    },
    [loadPostsAsync.rejected.type]: (state, { payload }: PayloadAction<string>) => {
      state.error = payload
      state.hasError = true
      state.isLoading = false
    },
  },
})

export const usePostsState = (): PostsState =>
  useSelector<AppRootState, PostsState>((state) => state.posts)

export const usePostsActions = (): PostsActions => {
  const dispatch = useDispatch<AppDispatch>()
  const { actions } = postsSlice

  return {
    createPost: (post: Post) => dispatch(actions.createPost(post)),
    loadPostsAsync: () => dispatch(loadPostsAsync()),
  }
}
