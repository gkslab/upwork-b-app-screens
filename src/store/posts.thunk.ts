import { createAsyncThunk } from '@reduxjs/toolkit'
import { API, graphqlOperation } from 'aws-amplify'
import { GetPostsFromPublicFeed } from '../graphql/customQueries'

export const loadPostsAsync = createAsyncThunk(
  'posts/loadPosts',
  async (_, { rejectWithValue }) => {
    try {
      const posts: any = await API.graphql(graphqlOperation(GetPostsFromPublicFeed))
      return posts
    } catch (error) {
      rejectWithValue(error)
    }
  },
)
