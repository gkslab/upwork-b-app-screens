import React, { FC } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

interface UserRowProps {
  name: string
  username: string
  avatar: string
}

const UserRow: FC<UserRowProps> = ({ name, username, avatar }) => {
  return (
    <View style={styles.container}>
      <Image source={{ uri: avatar }} style={styles.imgPlaceholder} />
      <View>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.bio}>{username}</Text>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 5,
  },
  imgPlaceholder: {
    backgroundColor: '#C4C4C4',
    width: 50,
    height: 50,
    borderRadius: 50,
    marginRight: 10,
  },
  name: {
    fontSize: 15,
    lineHeight: 19,
  },
  bio: {
    fontSize: 15,
    color: '#C4C4C4',
    lineHeight: 19,
  },
})

export default UserRow
