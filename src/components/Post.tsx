import { useNavigation } from '@react-navigation/native'
import { Storage } from 'aws-amplify'
import React, { FC } from 'react'
import { Dimensions, ImageBackground, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const { width } = Dimensions.get('window')
const cardWidth = width / 2 - 20

const Post: FC<{ item: any }> = ({ item }) => {
  const [thumbnail, setThumbnail] = React.useState('http://placehold.it/500')
  const thumbKey = item?.posts?.items[0]?.thumbnail

  const navigation = useNavigation()

  React.useEffect(() => {
    const fetchThumb = async () => {
      if (!thumbKey) return
      const uri = (await Storage.get(thumbKey)) as string
      setThumbnail(uri)
    }
    fetchThumb()
  }, [thumbKey])

  const sides: any[] = item?.sides || []

  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Home', { topicID: item?.id, prevScreen: 'publicPosts' })}
    >
      <ImageBackground style={styles.container} source={{ uri: thumbnail }}>
        <View style={styles.listContainer}>
          <View style={styles.textContainer}>
            <Text style={styles.title}>{item?.name}</Text>
          </View>
          <View style={styles.btnsContainer}>
            {sides.map((side, i) => (
              <View key={i} style={{ flex: 1 }}>
                <Text style={[styles.title, { textAlign: 'center' }]}>50</Text>
                <Text
                  style={[
                    styles.text,
                    {
                      backgroundColor: side?.color,
                    },
                  ]}
                >
                  {side?.side}
                </Text>
              </View>
            ))}
          </View>
        </View>
        <View style={styles.circle} />
      </ImageBackground>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  listContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  container: {
    width: cardWidth,
    height: 300,
    marginHorizontal: 5,
    borderRadius: 10,
    paddingHorizontal: 7,
    paddingVertical: 15,
    justifyContent: 'flex-end',
    marginBottom: 7,
    overflow: 'hidden',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  imgPlaceholder: {
    backgroundColor: '#C4C4C4',
    width: 50,
    height: 50,
    borderRadius: 50,
    marginRight: 10,
  },
  title: {
    fontSize: 13,
    color: '#fff',
  },
  circle: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 20,
    height: 20,
    backgroundColor: 'white',
    borderRadius: 50,
  },
  textContainer: { flex: 1 },
  btnsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  text: {
    fontSize: 7,
    fontWeight: 'bold',
    textAlign: 'center',
    marginHorizontal: 2,
    borderRadius: 10,
    paddingVertical: 1,
    overflow: 'hidden',
  },
})

export default React.memo(Post)
