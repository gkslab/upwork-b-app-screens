import { Storage } from 'aws-amplify'
import { AVPlaybackStatus, Video } from 'expo-av'
import * as React from 'react'
import { StyleSheet } from 'react-native'

interface PlaylistProps {
  keys: string[]
  shouldPlay?: boolean
}

const Playlist: React.FC<PlaylistProps> = (props) => {
  const { keys, shouldPlay } = props

  const videoRef = React.useRef<Video>(null)
  const [currentIndex, setCurrentIndex] = React.useState(0)
  const [videoURIs, setVideoURIs] = React.useState<string[]>([])

  React.useEffect(() => {
    const fetchVideoURIs = async () => {
      const URIs = (await Promise.all(keys.map(Storage.get))) as string[]
      setVideoURIs(URIs)
    }
    fetchVideoURIs()
  }, [keys])

  const handlePlayback = React.useCallback(
    async (playback: Video) => {
      try {
        await playback.loadAsync({ uri: videoURIs[currentIndex] })
        await playback.playAsync()
      } catch (error) {
        console.log('Initial Playlist Error', error)
      }
    },
    [currentIndex, videoURIs],
  )

  const handlePlaybackStatus = React.useCallback(
    async (status: AVPlaybackStatus) => {
      if (!status.isLoaded || !videoRef.current) return
      if (status.didJustFinish) {
        const playback = videoRef.current
        try {
          const nextIndex = currentIndex === videoURIs.length - 1 ? 0 : currentIndex + 1
          await playback.unloadAsync()
          await playback.loadAsync({ uri: videoURIs[nextIndex] })
          await playback.playAsync()
          setCurrentIndex(nextIndex)
        } catch (error) {
          console.log('Video Change Playlist Error', error)
        }
      }
    },
    [currentIndex, videoURIs],
  )

  React.useEffect(() => {
    if (videoRef.current) {
      const playbackObject = videoRef.current
      handlePlayback(playbackObject)
      playbackObject.setOnPlaybackStatusUpdate(handlePlaybackStatus)
    }
  }, [handlePlayback, handlePlaybackStatus])

  return (
    <Video
      ref={videoRef}
      shouldPlay={shouldPlay}
      rate={1.0}
      volume={1.0}
      resizeMode="cover"
      style={StyleSheet.absoluteFillObject}
    />
  )
}

export default Playlist
