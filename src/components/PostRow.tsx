import { useNavigation } from '@react-navigation/native'
import { Storage } from 'aws-amplify'
import React, { FC } from 'react'
import { Dimensions, ImageBackground, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const { width } = Dimensions.get('window')
const cardWidth = width

const PostRow: FC<{ item: any }> = ({ item }) => {
  const [thumbnail, setThumbnail] = React.useState('http://placehold.it/500')
  const thumbKey = item?.posts?.items[0]?.thumbnail

  const navigation = useNavigation()

  React.useEffect(() => {
    const fetchThumb = async () => {
      if (!thumbKey) return
      const uri = (await Storage.get(thumbKey)) as string
      setThumbnail(uri)
    }
    fetchThumb()
  }, [thumbKey])

  const sides: any[] = item?.sides || []

  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Home', { topicID: item?.id, prevScreen: 'privatePosts' })}
    >
      <ImageBackground resizeMode={'cover'} style={styles.container} source={{ uri: thumbnail }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'flex-end',
          }}
        >
          <View style={styles.textContainer}>
            <Text style={styles.title}>{item?.name}</Text>
          </View>
          <View style={styles.btnsContainer}>
            {sides.map((side, i) => (
              <View key={i} style={{ flex: 1 }}>
                <Text
                  style={[
                    styles.text,
                    {
                      backgroundColor: side?.color,
                    },
                  ]}
                >
                  {'(' + '50' + ') ' + side?.side}
                </Text>
              </View>
            ))}
          </View>
        </View>
        <View style={styles.circle} />
      </ImageBackground>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  container: {
    width: cardWidth + 1,
    height: 284,
    paddingHorizontal: 7,
    paddingVertical: 7,
    justifyContent: 'flex-end',
    marginBottom: 7,
  },
  imgPlaceholder: {
    backgroundColor: '#C4C4C4',
    width: 50,
    height: 50,
    borderRadius: 50,
    marginRight: 10,
  },
  title: {
    fontSize: 13,
    color: '#fff',
  },
  circle: {
    position: 'absolute',
    top: 10,
    right: 20,
    width: 20,
    height: 20,
    backgroundColor: 'white',
    borderRadius: 50,
  },
  textContainer: { flex: 1 },
  btnsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  text: {
    fontSize: 12,
    color: 'white',
    textAlign: 'center',
    marginHorizontal: 2,
    borderRadius: 10,
    paddingVertical: 1,
    overflow: 'hidden',
  },
})

export default PostRow
