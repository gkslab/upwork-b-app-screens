import React, { FC, useState } from 'react'
import { Image, StyleSheet, TextInput, View } from 'react-native'
const SearchInput: FC = () => {
  const [term, setTerm] = useState<string>('')
  return (
    <View style={styles.container}>
      <Image source={require('../assets/search.png')} style={{ width: 20, height: 30 }} />
      <TextInput
        value={term}
        onChangeText={(text) => setTerm(text)}
        placeholder={'Search'}
        style={styles.textStyle}
      />
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#C4C4C4',
    borderRadius: 10,
    paddingHorizontal: 5,
    marginVertical: 10,
  },
  textStyle: {
    fontSize: 15,
    paddingLeft: 10,
    flex: 1,
  },
})

export default SearchInput
