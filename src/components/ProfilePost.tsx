import { useNavigation } from '@react-navigation/native'
import { Storage } from 'aws-amplify'
import React, { FC } from 'react'
import { Dimensions, Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const { width } = Dimensions.get('window')

const cardWidth = width / 3 - 4

const ProfilePost: FC<{ item: any }> = ({ item }) => {
  const [thumbnail, setThumbnail] = React.useState('http://placehold.it/500')
  const thumbKey = item?.posts?.items[0]?.thumbnail

  const navigation = useNavigation()

  React.useEffect(() => {
    const fetchThumb = async () => {
      if (!thumbKey) return
      const uri = (await Storage.get(thumbKey)) as string
      setThumbnail(uri)
    }
    fetchThumb()
  }, [thumbKey])

  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Home', { topicID: item?.id, prevScreen: 'myTopics' })}
    >
      <ImageBackground style={styles.container} source={{ uri: thumbnail }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'flex-end',
          }}
        >
          <View style={styles.textContainer}>
            <Text style={styles.title}>{item?.name}</Text>
          </View>
          <View style={styles.btnsContainer}>
            <View style={styles.counterContainer}>
              <Text style={styles.number}>50</Text>
              <Image source={require('../assets/camera.png')} style={styles.img} />
            </View>
          </View>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  )
}
const styles = StyleSheet.create({
  container: {
    width: cardWidth,
    height: 220,
    paddingHorizontal: 7,
    paddingVertical: 7,
    marginHorizontal: 2,
    justifyContent: 'flex-end',
    marginBottom: 7,
    borderRadius: 10,
    overflow: 'hidden',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  imgPlaceholder: {
    backgroundColor: '#C4C4C4',
    width: 50,
    height: 50,
    borderRadius: 50,
    marginRight: 10,
  },
  title: {
    fontSize: 13,
    color: '#fff',
  },
  circle: {
    position: 'absolute',
    top: 10,
    right: 20,
    width: 20,
    height: 20,
    backgroundColor: 'white',
    borderRadius: 50,
  },
  textContainer: { flex: 1 },
  btnsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  number: {
    fontSize: 12,
    color: 'white',
    textAlign: 'center',
    marginHorizontal: 2,
    borderRadius: 10,
    paddingVertical: 1,
  },
  counterContainer: { flex: 1, alignItems: 'flex-end' },
  img: { width: 16, height: 13 },
})

export default ProfilePost
