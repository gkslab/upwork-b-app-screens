/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateUserProfileInput = {
  id?: string | null,
  username: string,
  private: boolean,
  name: string,
  dateOfBirth: string,
  avatar: string,
  description?: string | null,
  totalFollowing?: number | null,
  totalReactions?: number | null,
  totalPosts?: number | null,
};

export type ModelUserProfileConditionInput = {
  username?: ModelStringInput | null,
  private?: ModelBooleanInput | null,
  name?: ModelStringInput | null,
  dateOfBirth?: ModelStringInput | null,
  avatar?: ModelStringInput | null,
  description?: ModelStringInput | null,
  totalFollowing?: ModelIntInput | null,
  totalReactions?: ModelIntInput | null,
  totalPosts?: ModelIntInput | null,
  and?: Array< ModelUserProfileConditionInput | null > | null,
  or?: Array< ModelUserProfileConditionInput | null > | null,
  not?: ModelUserProfileConditionInput | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelBooleanInput = {
  ne?: boolean | null,
  eq?: boolean | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type ModelIntInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type UpdateUserProfileInput = {
  id: string,
  username?: string | null,
  private?: boolean | null,
  name?: string | null,
  dateOfBirth?: string | null,
  avatar?: string | null,
  description?: string | null,
  totalFollowing?: number | null,
  totalReactions?: number | null,
  totalPosts?: number | null,
};

export type DeleteUserProfileInput = {
  id?: string | null,
};

export type CreateFollowersInput = {
  id?: string | null,
  userID: string,
  requestor: string,
  approved?: boolean | null,
};

export type ModelFollowersConditionInput = {
  userID?: ModelIDInput | null,
  requestor?: ModelStringInput | null,
  approved?: ModelBooleanInput | null,
  and?: Array< ModelFollowersConditionInput | null > | null,
  or?: Array< ModelFollowersConditionInput | null > | null,
  not?: ModelFollowersConditionInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type UpdateFollowersInput = {
  id: string,
  userID?: string | null,
  requestor?: string | null,
  approved?: boolean | null,
};

export type DeleteFollowersInput = {
  id?: string | null,
};

export type CreateTopicInput = {
  id?: string | null,
  userID: string,
  name: string,
  description: string,
  sides: Array< SideInput >,
  likes?: number | null,
  thumbnail: string,
  tags?: Array< string | null > | null,
};

export type SideInput = {
  side: string,
  color: string,
};

export type ModelTopicConditionInput = {
  userID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  description?: ModelStringInput | null,
  likes?: ModelIntInput | null,
  thumbnail?: ModelStringInput | null,
  tags?: ModelStringInput | null,
  and?: Array< ModelTopicConditionInput | null > | null,
  or?: Array< ModelTopicConditionInput | null > | null,
  not?: ModelTopicConditionInput | null,
};

export type UpdateTopicInput = {
  id: string,
  userID?: string | null,
  name?: string | null,
  description?: string | null,
  sides?: Array< SideInput > | null,
  likes?: number | null,
  thumbnail?: string | null,
  tags?: Array< string | null > | null,
};

export type DeleteTopicInput = {
  id?: string | null,
};

export type CreatePostInput = {
  id?: string | null,
  userID: string,
  topicID: string,
  videoUrl: string,
  content?: string | null,
  flag?: number | null,
  likes?: number | null,
  thumbnail: string,
  side?: SideInput | null,
  tags?: Array< string | null > | null,
};

export type ModelPostConditionInput = {
  userID?: ModelIDInput | null,
  topicID?: ModelIDInput | null,
  videoUrl?: ModelStringInput | null,
  content?: ModelStringInput | null,
  flag?: ModelIntInput | null,
  likes?: ModelIntInput | null,
  thumbnail?: ModelStringInput | null,
  tags?: ModelStringInput | null,
  and?: Array< ModelPostConditionInput | null > | null,
  or?: Array< ModelPostConditionInput | null > | null,
  not?: ModelPostConditionInput | null,
};

export type UpdatePostInput = {
  id: string,
  userID?: string | null,
  topicID?: string | null,
  videoUrl?: string | null,
  content?: string | null,
  flag?: number | null,
  likes?: number | null,
  thumbnail?: string | null,
  side?: SideInput | null,
  tags?: Array< string | null > | null,
};

export type DeletePostInput = {
  id?: string | null,
};

export type CreateCommentInput = {
  id?: string | null,
  content: string,
  postID: string,
  updatedAt?: string | null,
};

export type ModelCommentConditionInput = {
  content?: ModelStringInput | null,
  postID?: ModelIDInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelCommentConditionInput | null > | null,
  or?: Array< ModelCommentConditionInput | null > | null,
  not?: ModelCommentConditionInput | null,
};

export type UpdateCommentInput = {
  id: string,
  content?: string | null,
  postID?: string | null,
  updatedAt?: string | null,
};

export type DeleteCommentInput = {
  id?: string | null,
};

export type CreateResponseInput = {
  id?: string | null,
  postID: string,
  responseCode: number,
  updatedAt?: string | null,
};

export type ModelResponseConditionInput = {
  postID?: ModelIDInput | null,
  responseCode?: ModelIntInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelResponseConditionInput | null > | null,
  or?: Array< ModelResponseConditionInput | null > | null,
  not?: ModelResponseConditionInput | null,
};

export type UpdateResponseInput = {
  id: string,
  postID?: string | null,
  responseCode?: number | null,
  updatedAt?: string | null,
};

export type DeleteResponseInput = {
  id?: string | null,
};

export type ModelUserProfileFilterInput = {
  id?: ModelIDInput | null,
  username?: ModelStringInput | null,
  private?: ModelBooleanInput | null,
  name?: ModelStringInput | null,
  dateOfBirth?: ModelStringInput | null,
  avatar?: ModelStringInput | null,
  description?: ModelStringInput | null,
  totalFollowing?: ModelIntInput | null,
  totalReactions?: ModelIntInput | null,
  totalPosts?: ModelIntInput | null,
  and?: Array< ModelUserProfileFilterInput | null > | null,
  or?: Array< ModelUserProfileFilterInput | null > | null,
  not?: ModelUserProfileFilterInput | null,
};

export type ModelFollowersFilterInput = {
  id?: ModelIDInput | null,
  userID?: ModelIDInput | null,
  requestor?: ModelStringInput | null,
  approved?: ModelBooleanInput | null,
  and?: Array< ModelFollowersFilterInput | null > | null,
  or?: Array< ModelFollowersFilterInput | null > | null,
  not?: ModelFollowersFilterInput | null,
};

export type ModelTopicFilterInput = {
  id?: ModelIDInput | null,
  userID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  description?: ModelStringInput | null,
  likes?: ModelIntInput | null,
  thumbnail?: ModelStringInput | null,
  tags?: ModelStringInput | null,
  and?: Array< ModelTopicFilterInput | null > | null,
  or?: Array< ModelTopicFilterInput | null > | null,
  not?: ModelTopicFilterInput | null,
};

export type ModelPostFilterInput = {
  id?: ModelIDInput | null,
  userID?: ModelIDInput | null,
  topicID?: ModelIDInput | null,
  videoUrl?: ModelStringInput | null,
  content?: ModelStringInput | null,
  flag?: ModelIntInput | null,
  likes?: ModelIntInput | null,
  thumbnail?: ModelStringInput | null,
  tags?: ModelStringInput | null,
  and?: Array< ModelPostFilterInput | null > | null,
  or?: Array< ModelPostFilterInput | null > | null,
  not?: ModelPostFilterInput | null,
};

export type ModelCommentFilterInput = {
  id?: ModelIDInput | null,
  content?: ModelStringInput | null,
  postID?: ModelIDInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelCommentFilterInput | null > | null,
  or?: Array< ModelCommentFilterInput | null > | null,
  not?: ModelCommentFilterInput | null,
};

export type ModelResponseFilterInput = {
  id?: ModelIDInput | null,
  postID?: ModelIDInput | null,
  responseCode?: ModelIntInput | null,
  updatedAt?: ModelStringInput | null,
  and?: Array< ModelResponseFilterInput | null > | null,
  or?: Array< ModelResponseFilterInput | null > | null,
  not?: ModelResponseFilterInput | null,
};

export type HomeQuery = {
  listTopics:  {
    __typename: "ModelTopicConnection",
    nextToken: string | null,
    items:  Array< {
      __typename: "Topic",
      id: string,
      name: string,
      sides:  Array< {
        __typename: "Side",
        color: string,
        side: string,
      } >,
      description: string,
      posts:  {
        __typename: "ModelPostConnection",
        nextToken: string | null,
        items:  Array< {
          __typename: "Post",
          user:  {
            __typename: "UserProfile",
            name: string,
          } | null,
          side:  {
            __typename: "Side",
            color: string,
            side: string,
          } | null,
          thumbnail: string,
          id: string,
          videoUrl: string,
        } | null > | null,
      } | null,
    } | null > | null,
  } | null,
};

export type HomePostsPaginationQueryVariables = {
  nextToken: string,
};

export type HomePostsPaginationQuery = {
  listPosts:  {
    __typename: "ModelPostConnection",
    items:  Array< {
      __typename: "Post",
      user:  {
        __typename: "UserProfile",
        name: string,
      } | null,
      side:  {
        __typename: "Side",
        color: string,
        side: string,
      } | null,
      thumbnail: string,
      id: string,
      videoUrl: string,
    } | null > | null,
  } | null,
};

export type HomeTopicPaginationQueryVariables = {
  nextToken: string,
};

export type HomeTopicPaginationQuery = {
  listTopics:  {
    __typename: "ModelTopicConnection",
    nextToken: string | null,
    items:  Array< {
      __typename: "Topic",
      name: string,
      description: string,
      posts:  {
        __typename: "ModelPostConnection",
        nextToken: string | null,
        items:  Array< {
          __typename: "Post",
          user:  {
            __typename: "UserProfile",
            name: string,
          } | null,
          side:  {
            __typename: "Side",
            color: string,
            side: string,
          } | null,
          thumbnail: string,
          id: string,
          videoUrl: string,
        } | null > | null,
      } | null,
    } | null > | null,
  } | null,
};

export type CreateUserProfileMutationVariables = {
  input: CreateUserProfileInput,
  condition?: ModelUserProfileConditionInput | null,
};

export type CreateUserProfileMutation = {
  createUserProfile:  {
    __typename: "UserProfile",
    id: string,
    username: string,
    private: boolean,
    name: string,
    dateOfBirth: string,
    avatar: string,
    description: string | null,
    totalFollowing: number | null,
    totalReactions: number | null,
    totalPosts: number | null,
    topics:  {
      __typename: "ModelTopicConnection",
      nextToken: string | null,
    } | null,
    followRequests:  {
      __typename: "ModelFollowersConnection",
      nextToken: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type UpdateUserProfileMutationVariables = {
  input: UpdateUserProfileInput,
  condition?: ModelUserProfileConditionInput | null,
};

export type UpdateUserProfileMutation = {
  updateUserProfile:  {
    __typename: "UserProfile",
    id: string,
    username: string,
    private: boolean,
    name: string,
    dateOfBirth: string,
    avatar: string,
    description: string | null,
    totalFollowing: number | null,
    totalReactions: number | null,
    totalPosts: number | null,
    topics:  {
      __typename: "ModelTopicConnection",
      nextToken: string | null,
    } | null,
    followRequests:  {
      __typename: "ModelFollowersConnection",
      nextToken: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type DeleteUserProfileMutationVariables = {
  input: DeleteUserProfileInput,
  condition?: ModelUserProfileConditionInput | null,
};

export type DeleteUserProfileMutation = {
  deleteUserProfile:  {
    __typename: "UserProfile",
    id: string,
    username: string,
    private: boolean,
    name: string,
    dateOfBirth: string,
    avatar: string,
    description: string | null,
    totalFollowing: number | null,
    totalReactions: number | null,
    totalPosts: number | null,
    topics:  {
      __typename: "ModelTopicConnection",
      nextToken: string | null,
    } | null,
    followRequests:  {
      __typename: "ModelFollowersConnection",
      nextToken: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type CreateFollowersMutationVariables = {
  input: CreateFollowersInput,
  condition?: ModelFollowersConditionInput | null,
};

export type CreateFollowersMutation = {
  createFollowers:  {
    __typename: "Followers",
    id: string,
    userID: string,
    requestor: string,
    approved: boolean | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedAt: string,
    owner: string | null,
  } | null,
};

export type UpdateFollowersMutationVariables = {
  input: UpdateFollowersInput,
  condition?: ModelFollowersConditionInput | null,
};

export type UpdateFollowersMutation = {
  updateFollowers:  {
    __typename: "Followers",
    id: string,
    userID: string,
    requestor: string,
    approved: boolean | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedAt: string,
    owner: string | null,
  } | null,
};

export type DeleteFollowersMutationVariables = {
  input: DeleteFollowersInput,
  condition?: ModelFollowersConditionInput | null,
};

export type DeleteFollowersMutation = {
  deleteFollowers:  {
    __typename: "Followers",
    id: string,
    userID: string,
    requestor: string,
    approved: boolean | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedAt: string,
    owner: string | null,
  } | null,
};

export type CreateTopicMutationVariables = {
  input: CreateTopicInput,
  condition?: ModelTopicConditionInput | null,
};

export type CreateTopicMutation = {
  createTopic:  {
    __typename: "Topic",
    id: string,
    userID: string,
    name: string,
    description: string,
    sides:  Array< {
      __typename: "Side",
      side: string,
      color: string,
    } >,
    likes: number | null,
    thumbnail: string,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type UpdateTopicMutationVariables = {
  input: UpdateTopicInput,
  condition?: ModelTopicConditionInput | null,
};

export type UpdateTopicMutation = {
  updateTopic:  {
    __typename: "Topic",
    id: string,
    userID: string,
    name: string,
    description: string,
    sides:  Array< {
      __typename: "Side",
      side: string,
      color: string,
    } >,
    likes: number | null,
    thumbnail: string,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type DeleteTopicMutationVariables = {
  input: DeleteTopicInput,
  condition?: ModelTopicConditionInput | null,
};

export type DeleteTopicMutation = {
  deleteTopic:  {
    __typename: "Topic",
    id: string,
    userID: string,
    name: string,
    description: string,
    sides:  Array< {
      __typename: "Side",
      side: string,
      color: string,
    } >,
    likes: number | null,
    thumbnail: string,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type CreatePostMutationVariables = {
  input: CreatePostInput,
  condition?: ModelPostConditionInput | null,
};

export type CreatePostMutation = {
  createPost:  {
    __typename: "Post",
    id: string,
    userID: string,
    topicID: string,
    videoUrl: string,
    content: string | null,
    flag: number | null,
    likes: number | null,
    thumbnail: string,
    side:  {
      __typename: "Side",
      side: string,
      color: string,
    } | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    topic:  {
      __typename: "Topic",
      id: string,
      userID: string,
      name: string,
      description: string,
      likes: number | null,
      thumbnail: string,
      tags: Array< string | null > | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    responses:  {
      __typename: "ModelResponseConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type UpdatePostMutationVariables = {
  input: UpdatePostInput,
  condition?: ModelPostConditionInput | null,
};

export type UpdatePostMutation = {
  updatePost:  {
    __typename: "Post",
    id: string,
    userID: string,
    topicID: string,
    videoUrl: string,
    content: string | null,
    flag: number | null,
    likes: number | null,
    thumbnail: string,
    side:  {
      __typename: "Side",
      side: string,
      color: string,
    } | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    topic:  {
      __typename: "Topic",
      id: string,
      userID: string,
      name: string,
      description: string,
      likes: number | null,
      thumbnail: string,
      tags: Array< string | null > | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    responses:  {
      __typename: "ModelResponseConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type DeletePostMutationVariables = {
  input: DeletePostInput,
  condition?: ModelPostConditionInput | null,
};

export type DeletePostMutation = {
  deletePost:  {
    __typename: "Post",
    id: string,
    userID: string,
    topicID: string,
    videoUrl: string,
    content: string | null,
    flag: number | null,
    likes: number | null,
    thumbnail: string,
    side:  {
      __typename: "Side",
      side: string,
      color: string,
    } | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    topic:  {
      __typename: "Topic",
      id: string,
      userID: string,
      name: string,
      description: string,
      likes: number | null,
      thumbnail: string,
      tags: Array< string | null > | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    responses:  {
      __typename: "ModelResponseConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type CreateCommentMutationVariables = {
  input: CreateCommentInput,
  condition?: ModelCommentConditionInput | null,
};

export type CreateCommentMutation = {
  createComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    postID: string,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type UpdateCommentMutationVariables = {
  input: UpdateCommentInput,
  condition?: ModelCommentConditionInput | null,
};

export type UpdateCommentMutation = {
  updateComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    postID: string,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type DeleteCommentMutationVariables = {
  input: DeleteCommentInput,
  condition?: ModelCommentConditionInput | null,
};

export type DeleteCommentMutation = {
  deleteComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    postID: string,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type CreateResponseMutationVariables = {
  input: CreateResponseInput,
  condition?: ModelResponseConditionInput | null,
};

export type CreateResponseMutation = {
  createResponse:  {
    __typename: "Response",
    id: string,
    postID: string,
    responseCode: number,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type UpdateResponseMutationVariables = {
  input: UpdateResponseInput,
  condition?: ModelResponseConditionInput | null,
};

export type UpdateResponseMutation = {
  updateResponse:  {
    __typename: "Response",
    id: string,
    postID: string,
    responseCode: number,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type DeleteResponseMutationVariables = {
  input: DeleteResponseInput,
  condition?: ModelResponseConditionInput | null,
};

export type DeleteResponseMutation = {
  deleteResponse:  {
    __typename: "Response",
    id: string,
    postID: string,
    responseCode: number,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type GetUserProfileQueryVariables = {
  id: string,
};

export type GetUserProfileQuery = {
  getUserProfile:  {
    __typename: "UserProfile",
    id: string,
    username: string,
    private: boolean,
    name: string,
    dateOfBirth: string,
    avatar: string,
    description: string | null,
    totalFollowing: number | null,
    totalReactions: number | null,
    totalPosts: number | null,
    topics:  {
      __typename: "ModelTopicConnection",
      nextToken: string | null,
    } | null,
    followRequests:  {
      __typename: "ModelFollowersConnection",
      nextToken: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type ListUserProfilesQueryVariables = {
  filter?: ModelUserProfileFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListUserProfilesQuery = {
  listUserProfiles:  {
    __typename: "ModelUserProfileConnection",
    items:  Array< {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetFollowersQueryVariables = {
  id: string,
};

export type GetFollowersQuery = {
  getFollowers:  {
    __typename: "Followers",
    id: string,
    userID: string,
    requestor: string,
    approved: boolean | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedAt: string,
    owner: string | null,
  } | null,
};

export type ListFollowerssQueryVariables = {
  filter?: ModelFollowersFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListFollowerssQuery = {
  listFollowerss:  {
    __typename: "ModelFollowersConnection",
    items:  Array< {
      __typename: "Followers",
      id: string,
      userID: string,
      requestor: string,
      approved: boolean | null,
      createdOn: string,
      updatedAt: string,
      owner: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetTopicQueryVariables = {
  id: string,
};

export type GetTopicQuery = {
  getTopic:  {
    __typename: "Topic",
    id: string,
    userID: string,
    name: string,
    description: string,
    sides:  Array< {
      __typename: "Side",
      side: string,
      color: string,
    } >,
    likes: number | null,
    thumbnail: string,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type ListTopicsQueryVariables = {
  filter?: ModelTopicFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListTopicsQuery = {
  listTopics:  {
    __typename: "ModelTopicConnection",
    items:  Array< {
      __typename: "Topic",
      id: string,
      userID: string,
      name: string,
      description: string,
      likes: number | null,
      thumbnail: string,
      tags: Array< string | null > | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetPostQueryVariables = {
  id: string,
};

export type GetPostQuery = {
  getPost:  {
    __typename: "Post",
    id: string,
    userID: string,
    topicID: string,
    videoUrl: string,
    content: string | null,
    flag: number | null,
    likes: number | null,
    thumbnail: string,
    side:  {
      __typename: "Side",
      side: string,
      color: string,
    } | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    topic:  {
      __typename: "Topic",
      id: string,
      userID: string,
      name: string,
      description: string,
      likes: number | null,
      thumbnail: string,
      tags: Array< string | null > | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    responses:  {
      __typename: "ModelResponseConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type ListPostsQueryVariables = {
  filter?: ModelPostFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListPostsQuery = {
  listPosts:  {
    __typename: "ModelPostConnection",
    items:  Array< {
      __typename: "Post",
      id: string,
      userID: string,
      topicID: string,
      videoUrl: string,
      content: string | null,
      flag: number | null,
      likes: number | null,
      thumbnail: string,
      tags: Array< string | null > | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetCommentQueryVariables = {
  id: string,
};

export type GetCommentQuery = {
  getComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    postID: string,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type ListCommentsQueryVariables = {
  filter?: ModelCommentFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCommentsQuery = {
  listComments:  {
    __typename: "ModelCommentConnection",
    items:  Array< {
      __typename: "Comment",
      id: string,
      content: string,
      postID: string,
      updatedAt: string,
      createdAt: string,
      owner: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type GetResponseQueryVariables = {
  id: string,
};

export type GetResponseQuery = {
  getResponse:  {
    __typename: "Response",
    id: string,
    postID: string,
    responseCode: number,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type ListResponsesQueryVariables = {
  filter?: ModelResponseFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListResponsesQuery = {
  listResponses:  {
    __typename: "ModelResponseConnection",
    items:  Array< {
      __typename: "Response",
      id: string,
      postID: string,
      responseCode: number,
      updatedAt: string,
      createdAt: string,
      owner: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateUserProfileSubscription = {
  onCreateUserProfile:  {
    __typename: "UserProfile",
    id: string,
    username: string,
    private: boolean,
    name: string,
    dateOfBirth: string,
    avatar: string,
    description: string | null,
    totalFollowing: number | null,
    totalReactions: number | null,
    totalPosts: number | null,
    topics:  {
      __typename: "ModelTopicConnection",
      nextToken: string | null,
    } | null,
    followRequests:  {
      __typename: "ModelFollowersConnection",
      nextToken: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type OnUpdateUserProfileSubscription = {
  onUpdateUserProfile:  {
    __typename: "UserProfile",
    id: string,
    username: string,
    private: boolean,
    name: string,
    dateOfBirth: string,
    avatar: string,
    description: string | null,
    totalFollowing: number | null,
    totalReactions: number | null,
    totalPosts: number | null,
    topics:  {
      __typename: "ModelTopicConnection",
      nextToken: string | null,
    } | null,
    followRequests:  {
      __typename: "ModelFollowersConnection",
      nextToken: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type OnDeleteUserProfileSubscription = {
  onDeleteUserProfile:  {
    __typename: "UserProfile",
    id: string,
    username: string,
    private: boolean,
    name: string,
    dateOfBirth: string,
    avatar: string,
    description: string | null,
    totalFollowing: number | null,
    totalReactions: number | null,
    totalPosts: number | null,
    topics:  {
      __typename: "ModelTopicConnection",
      nextToken: string | null,
    } | null,
    followRequests:  {
      __typename: "ModelFollowersConnection",
      nextToken: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type OnCreateFollowersSubscription = {
  onCreateFollowers:  {
    __typename: "Followers",
    id: string,
    userID: string,
    requestor: string,
    approved: boolean | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedAt: string,
    owner: string | null,
  } | null,
};

export type OnUpdateFollowersSubscription = {
  onUpdateFollowers:  {
    __typename: "Followers",
    id: string,
    userID: string,
    requestor: string,
    approved: boolean | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedAt: string,
    owner: string | null,
  } | null,
};

export type OnDeleteFollowersSubscription = {
  onDeleteFollowers:  {
    __typename: "Followers",
    id: string,
    userID: string,
    requestor: string,
    approved: boolean | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedAt: string,
    owner: string | null,
  } | null,
};

export type OnCreateTopicSubscription = {
  onCreateTopic:  {
    __typename: "Topic",
    id: string,
    userID: string,
    name: string,
    description: string,
    sides:  Array< {
      __typename: "Side",
      side: string,
      color: string,
    } >,
    likes: number | null,
    thumbnail: string,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type OnUpdateTopicSubscription = {
  onUpdateTopic:  {
    __typename: "Topic",
    id: string,
    userID: string,
    name: string,
    description: string,
    sides:  Array< {
      __typename: "Side",
      side: string,
      color: string,
    } >,
    likes: number | null,
    thumbnail: string,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type OnDeleteTopicSubscription = {
  onDeleteTopic:  {
    __typename: "Topic",
    id: string,
    userID: string,
    name: string,
    description: string,
    sides:  Array< {
      __typename: "Side",
      side: string,
      color: string,
    } >,
    likes: number | null,
    thumbnail: string,
    posts:  {
      __typename: "ModelPostConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type OnCreatePostSubscription = {
  onCreatePost:  {
    __typename: "Post",
    id: string,
    userID: string,
    topicID: string,
    videoUrl: string,
    content: string | null,
    flag: number | null,
    likes: number | null,
    thumbnail: string,
    side:  {
      __typename: "Side",
      side: string,
      color: string,
    } | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    topic:  {
      __typename: "Topic",
      id: string,
      userID: string,
      name: string,
      description: string,
      likes: number | null,
      thumbnail: string,
      tags: Array< string | null > | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    responses:  {
      __typename: "ModelResponseConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type OnUpdatePostSubscription = {
  onUpdatePost:  {
    __typename: "Post",
    id: string,
    userID: string,
    topicID: string,
    videoUrl: string,
    content: string | null,
    flag: number | null,
    likes: number | null,
    thumbnail: string,
    side:  {
      __typename: "Side",
      side: string,
      color: string,
    } | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    topic:  {
      __typename: "Topic",
      id: string,
      userID: string,
      name: string,
      description: string,
      likes: number | null,
      thumbnail: string,
      tags: Array< string | null > | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    responses:  {
      __typename: "ModelResponseConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type OnDeletePostSubscription = {
  onDeletePost:  {
    __typename: "Post",
    id: string,
    userID: string,
    topicID: string,
    videoUrl: string,
    content: string | null,
    flag: number | null,
    likes: number | null,
    thumbnail: string,
    side:  {
      __typename: "Side",
      side: string,
      color: string,
    } | null,
    user:  {
      __typename: "UserProfile",
      id: string,
      username: string,
      private: boolean,
      name: string,
      dateOfBirth: string,
      avatar: string,
      description: string | null,
      totalFollowing: number | null,
      totalReactions: number | null,
      totalPosts: number | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    topic:  {
      __typename: "Topic",
      id: string,
      userID: string,
      name: string,
      description: string,
      likes: number | null,
      thumbnail: string,
      tags: Array< string | null > | null,
      createdOn: string,
      updatedOn: string,
      owner: string | null,
    } | null,
    comments:  {
      __typename: "ModelCommentConnection",
      nextToken: string | null,
    } | null,
    responses:  {
      __typename: "ModelResponseConnection",
      nextToken: string | null,
    } | null,
    tags: Array< string | null > | null,
    createdOn: string,
    updatedOn: string,
    owner: string | null,
  } | null,
};

export type OnCreateCommentSubscription = {
  onCreateComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    postID: string,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type OnUpdateCommentSubscription = {
  onUpdateComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    postID: string,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type OnDeleteCommentSubscription = {
  onDeleteComment:  {
    __typename: "Comment",
    id: string,
    content: string,
    postID: string,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type OnCreateResponseSubscription = {
  onCreateResponse:  {
    __typename: "Response",
    id: string,
    postID: string,
    responseCode: number,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type OnUpdateResponseSubscription = {
  onUpdateResponse:  {
    __typename: "Response",
    id: string,
    postID: string,
    responseCode: number,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};

export type OnDeleteResponseSubscription = {
  onDeleteResponse:  {
    __typename: "Response",
    id: string,
    postID: string,
    responseCode: number,
    updatedAt: string,
    createdAt: string,
    owner: string | null,
  } | null,
};
