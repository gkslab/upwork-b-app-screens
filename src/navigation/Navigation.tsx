import { createStackNavigator } from '@react-navigation/stack'
import React, { FC } from 'react'
import PostTopic from '../screens/PostTopic'
import Response from '../screens/Response'
import BottomBar from './BottomBar'

const Stack = createStackNavigator()

const Navigation: FC = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="BottomBar" component={BottomBar} />
      <Stack.Screen name="Response" component={Response} />
      <Stack.Screen name="PostTopic" component={PostTopic} />
    </Stack.Navigator>
  )
}
export default Navigation
