import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import React, { FC } from 'react'
import { Image } from 'react-native'
import Home from '../screens/Home'
import Profile from '../screens/Profile'
import Search from '../screens/Search'
import Settings from '../screens/Settings'
import StartTopic from '../screens/StartTopic'
const Tab = createBottomTabNavigator()

const BottomBar: FC = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        tabStyle: { backgroundColor: 'black' },
        showLabel: false,
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          unmountOnBlur: true,
          tabBarIcon: () => (
            <Image
              source={require('../assets/bottomBarIcons/home.png')}
              style={{ width: 29, height: 30 }}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarIcon: () => (
            <Image
              source={require('../assets/bottomBarIcons/search.png')}
              style={{ width: 29, height: 30 }}
            />
          ),
        }}
      />
      <Tab.Screen
        name="StartTopic"
        component={StartTopic}
        options={() => ({
          tabBarVisible: false,
          unmountOnBlur: true,
          tabBarIcon: () => (
            <Image
              source={require('../assets/bottomBarIcons/plus.png')}
              style={{ width: 29, height: 30 }}
            />
          ),
        })}
      />
      <Tab.Screen
        name="Settings"
        component={Settings}
        options={{
          tabBarIcon: () => (
            <Image
              source={require('../assets/bottomBarIcons/bell.png')}
              style={{ width: 29, height: 30 }}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: () => (
            <Image
              source={require('../assets/bottomBarIcons/user.png')}
              style={{ width: 29, height: 30 }}
            />
          ),
        }}
      />
    </Tab.Navigator>
  )
}
export default BottomBar
